require 'libsvm'

class SVM

  PARAM_CACHE_SIZE = 100
  PARAM_EPS = 0.001
  PARAM_C = 10

  attr_accessor :model_name, :model

  def initialize(model_name)
    @model_name = model_name
    @model = (File.exists? model_path) ? Libsvm::Model.load(model_path) : nil
  end

  def exists?
    !@model.nil?
  end

  # train model and save
  def train_with_probability(examples, labels)
    problem = Libsvm::Problem.new
    parameter = Libsvm::SvmParameter.new

    parameter.probability = 1
    parameter.cache_size = PARAM_CACHE_SIZE
    parameter.eps = PARAM_EPS
    parameter.c = PARAM_C

    examples.map! { |ary| Libsvm::Node.features(ary) }
    problem.set_examples(labels, examples)
    model = Libsvm::Model.train(problem, parameter)
    model.save(model_path)
  end

  def predict_probability(features)
    return nil unless @model
    probability = @model.predict_probability Libsvm::Node.features(features)
    probability.last.last
  end

  def predict_label(features)
    return nil unless @model
    @model.predict Libsvm::Node.features(features)
  end

  def model_path
    "data/models/#{@model_name}.model"
  end

end

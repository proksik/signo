require 'statsample'

class CorrelCoeff

  attr_accessor :coefficients, :model_name

  def initialize(model_name)
    @model_name = model_name
    load_from_file
  end

  def exists?
    !@coefficients.nil?
  end

  def properties
    return nil unless exists?
    @coefficients.keys
  end

  def load_from_file
    return nil unless File.exists? model_path
    f = File.open(model_path)
    content = f.read
    f.close
    @coefficients = {}
    content.split("\n").each do |line|
      items = line.split("\t")
      key = items.first.to_sym
      value = items.last.to_f
      @coefficients[key] = value
    end
  end

  def save_to_file
    return unless @coefficients
    f = File.open(model_path, 'w')
    @coefficients.each_pair do |key, value|
      f.write "#{key}\t#{value}\n"
    end
    f.close
  end

  def calculate_and_save(coefficients_data, labels, properties)
    labels_scale = labels.to_scale
    @coefficients = {}
    properties.each_with_index do |property_name, index|
      @coefficients[property_name] = normalize_coefficient Statsample::Bivariate.pearson(coefficients_data.map { |features| features[index] }.to_scale, labels_scale)
    end
    save_to_file
    @coefficients
  end

  def normalize_coefficient(coefficient)
    return 0.0 if coefficient.nan? || coefficient < 0.0
    coefficient
  end

  def model_path
    "data/models/#{@model_name}.correl_coeff"
  end

end

class ProgressBar

  attr_accessor :total_count, :actual_count

  FREQUENCY_STATUS = 100

  def initialize(total_count)
    @total_count = total_count
    @actual_count = 0
  end

  def increment
    @actual_count+= 1
    print_status if @actual_count % FREQUENCY_STATUS == 0
  end

  def print_status
    puts "#{@actual_count}/#{@total_count} - #{ (100.0 * @actual_count.to_f / @total_count.to_f).round(2) }%"
  end

end

require 'neography'

class Neo4jSignoPlugin

  def self.k_nearest_nodes(node, total_count)
    neo = Neography::Rest.new
    ext = Neography::Rest::Extensions.new neo.connection
    JSON.parse(ext.post("/ext/SiGNoPlugin/node/#{node.neo_id}/nearest_nodes", {count: total_count})).map{|neo_id| Neography::Node.load(neo_id) }
  end

end
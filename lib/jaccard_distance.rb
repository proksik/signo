module JaccardDistance

  def jaccard_ratio(intersect, union)
    return 0.0 if union.eql? 0
    intersect.to_f / union.to_f
  end

	def jaccard_ratio_for(array_1, array_2)
    jaccard_ratio (array_1 & array_2).count, (array_1 | array_2).count
	end

end

# encoding: utf-8
namespace :gov do

  # extracting

  # rake gov:extract
  task :extract => :environment do
    s = GOV::Extracting::Service.new
    s.extract_all
  end

  # rake gov:extract_organizations_1000
  task :extract_organizations_1000 => :environment do
    s = GOV::Extracting::Service.new
    s.extract_organizations('organizations1000')
  end

  # rake gov:extract_organizations_5000
  task :extract_organizations_5000 => :environment do
    s = GOV::Extracting::Service.new
    s.extract_organizations('organizations5000')
  end

  # rake gov:extract_organizations_10000
  task :extract_organizations_10000 => :environment do
    s = GOV::Extracting::Service.new
    s.extract_organizations('organizations10000')
  end

  # rake gov:extract_depend_organizations
  task :extract_depend_organizations => :environment do
    s = GOV::Extracting::Service.new
    s.extract_depend_organizations
  end

  # rake gov:experiment count=1000
  task :experiment => :environment do
    count = ENV['count'] ? ENV['count'].to_i : 1000
    exp = GOV::Experiments::Experiment.new count
    exp.train_properties
    exp.train_sgn
    exp.evaluate false
    exp.evaluate true
    exp.evaluate_naive
  end
end
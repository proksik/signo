# encoding: utf-8
namespace :annota do

  # rake annota:extract version=23_11_2013
  task :extract => :environment do
    version = ENV['version'] ? ENV['version'] : 'version'
    exp = Annota::Extracting::Service.new
    exp.extract version
  end

  # Experiments

  # rake annota:experiment3
  task :experiment3 => :environment do
    exp = Annota::Experiments::Experiment3.new
    puts '3 - NoCaching'
    exp.process_performance false
    puts '3 - Caching'
    exp.process_performance true
  end

  # 4n - [100,250] - nxn, 0.8
  # 4t - [100,250,500] - nxn, 0.8
  #    - [1000,5000]   - nx10, 0.5
  # 4t_sgn - [100,250,?] - nxn, 0.8
  #        - [1000,5000]   - nx10, 0.5

  # rake annota:experiments4 count=100
  task :experiments4 => :environment do
    count = ENV['count'] ? ENV['count'].to_i : 100
    exp = Annota::Experiments::Experiment4n.new(count)
    puts '4n - Preprocessing'
    exp.preprocessing
    puts '4n - Calculate SGN'
    exp.calculate_sgn
    puts '4n - Evaluate'
    exp.evaluate
    puts '4n - Summary'
    exp.summary_results
    exp = Annota::Experiments::Experiment4t.new(count, 0.5)
    puts '4t - Preprocessing'
    exp.preprocessing
    puts '4t - Train SNP'
    exp.train_properties
    puts '4t - Train SGN'
    exp.train_sgn
    puts '4t - Evaluate'
    exp.evaluate
    exp = Annota::Experiments::Experiment4tSGN.new(count, 0.8, {generate_temp_entities: false, generate_pairs_type: :nxn})
    puts '4tSGN - Preprocessing'
    exp.preprocessing
    puts '4tSGN - Train SGN'
    #exp.train_sgn
    puts '4tSGN - Evaluate'
    exp.evaluate
  end

  # rake annota:experiment5_one count=100
  task :experiment5_one => :environment do
    count = ENV['count'] ? ENV['count'].to_i : 100
    puts '5 - Initialize'
    exp = Annota::Experiments::Experiment5.new count, {generate_temp_entities: true, generate_pairs_type: :nx10}
    puts "5 - Count Test data: #{exp.test_data.count}"
    puts '5 - Preprocessing'
    exp.preprocessing
    puts '5 - Train SGN'
    exp.train_sgn
    puts '5 - Evaluation'
    exp.evaluate
  end

  # rake annota:delete_temp_nodes
  task :delete_temp_nodes => :environment do
    cleaner = Annota::Cleaner.new
    cleaner.delete_temp_nodes
  end

  # rake annota:experiment5_all
  task :experiment5_all => :environment do
    [5000, 1000, 500, 250, 100].each do |count|
      puts '5 - Initialize'
      exp = Annota::Experiments::Experiment5.new count, {generate_temp_entities: true, generate_pairs_type: :nx10}
      puts "5 - Count Test data: #{exp.test_data.count}"
      puts '5 - Preprocessing'
      exp.preprocessing
      puts '5 - Train SGN'
      exp.train_sgn
      puts '5 - Evaluation'
      exp.evaluate
    end
  end

end
task :stats => "signo:statsetup"

namespace :signo do
  task :statsetup do
    require 'rails/code_statistics'
    ::STATS_DIRECTORIES << ["Services", "app/services"]
    ::STATS_DIRECTORIES << ["Spec support", "spec/support"]
    ::STATS_DIRECTORIES << ["Spec factories", "spec/factories"]
  end
end
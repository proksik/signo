# encoding: utf-8
namespace :dup_detector do

  # need set dataset_name=name_for_dataset
  # rake dup_detector:extract dataset_name=annota_05_05
  task :extract => :environment do
    dataset_name = ENV['dataset_name'] ? ENV['dataset_name'] : 'dataset_name'
    ext = DupDetector::Extracting.new
    ext.extract dataset_name
  end

  # count_replications=100 dataset_name=name_for_dataset replication_prefix=prefix_for_entity_uri_in_temp_nodes
  # rake dup_detector:preprocessing count_replications=50 dataset_name=annota_05_05 replication_prefix=<http://annota.fiit.stuba.sk/ontology#person
  # rake dup_detector:preprocessing count_replications=50 dataset_name=annota_05_05
  task :preprocessing => :environment do
    dataset_name = ENV['dataset_name'] ? ENV['dataset_name'] : 'dataset_name'
    count = ENV['count_replications'] ? ENV['count_replications'].to_i : 100
    replication_prefix = ENV['replication_prefix'] ? ENV['replication_prefix'] : nil
    dd = DupDetector::Preprocessing.new dataset_name, count, replication_prefix
    puts 'DupDetector - Preprocessing'
    dd.preprocessing
    puts 'DupDetector - Train SGN'
    dd.train_sgn
  end


  # count dataset_name=name_for_dataset duplicates_file=file_in_dataset_dir_with_duplicates
  # rake dup_detector:find_duplicates dataset_name=annota_05_05 duplicates_file=duplicates.txt
  task :find_duplicates => :environment do
    dataset_name = ENV['dataset_name'] ? ENV['dataset_name'] : 'dataset_name'
    duplicates_file = ENV['duplicates_file'] ? ENV['duplicates_file'] : 'duplicates_file'
    dd = DupDetector::Evaluation.new dataset_name
    dd.find_duplicates duplicates_file
  end

end
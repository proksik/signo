module Enumerable

  def combinations
    1.upto(self.size).flat_map do |n|
      self.combination(n).to_a
    end
  end

end
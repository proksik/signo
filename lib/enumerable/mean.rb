module Enumerable

  def mean
  	self.sum.to_f / self.count.to_f
  end

end
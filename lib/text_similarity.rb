# encoding: utf-8
require 'n_gram'
require 'levenshtein'
require 'jaccard_distance'

module TextSimilarity
  include JaccardDistance

  #koefiticent podbonosti
  def n_gram_ratio(str1, str2, n=3)
    str1 = change_chars(str1, [:diacritic, :downcase, :space, :stop])
    str2 = change_chars(str2, [:diacritic, :downcase, :space, :stop])
    n_gram = NGram.new([str1, str2], :n => n)
    union = n_gram.ngrams_of_all_data[n].size
    intersect = 0
    n_gram.ngrams_of_all_data[n].each do |item|
      intersect+= 1 if item.second >= 2
    end
    jaccard_ratio(intersect, union)
  end

  def levenshtein_ratio(str1, str2)
    str1 = change_chars(str1, [:downcase, :diacritic, :space, :stop])
    str2 = change_chars(str2, [:downcase, :diacritic, :space, :stop])
    distance = Levenshtein.distance(str1, str2)
    max_distance = [str1.size, str2.size].max
    jaccard_ratio(max_distance-distance, max_distance)
  end

  #odstranime znaky podla nastaveni
  def change_chars(str, options = {})
    return "" if str.nil?
    options.each do |option|
      str = str.downcase if option.eql? :downcase
      str = remove_diacritics(str) if option.eql? :diacritic
      str = remove_space(str) if option.eql? :space
      str = remove_stop_char(str) if option.eql? :stop
    end
    str
  end

  private

  DIACRITICS_SEARCH = [:ç, :æ, :œ, :á, :é, :í, :ó, :ú, :a, :e, :i, :o, :u, :ä, :ë, :i, :ö, :ü, :y, :ý, :â, :e, :î, :ô, :u, :a, :e, :i, :o, :u, :č, :š, :ž, :ň, :š, :ň, :ú, :ť, :ľ, :Á, :É, :Í, :Ó, :Ú, :Ŕ, :Č, :Ď, :Ĺ, :Ľ, :Ň, :Š, :Ť, :Ř, :Ž, :Ý]
  DIACRITICS_REPLACE = [:c, :ae, :oe, :a, :e, :i, :o, :u, :a, :e, :i, :o, :u, :a, :e, :i, :o, :u, :y, :y, :a, :e, :i, :o, :u, :a, :e, :i, :o, :u, :c, :s, :z, :n, :s, :n, :u, :t, :l, :A, :E, :I, :O, :U, :R, :C, :D, :L, :L, :N, :S, :T, :R, :Z, :Y]

  def remove_diacritics(str)
    DIACRITICS_SEARCH.each_with_index do |search, index|
      replace = DIACRITICS_REPLACE[index]
      str = str.gsub(search.to_s, replace.to_s)
    end
    str
  end

  def remove_space(str)
    str.gsub(/\s+/, "")
  end

  def remove_stop_char(str)
    str.gsub(/[^0-9a-z]/i, '')
  end
end


class ArraysCache

  attr_accessor :cache, :key_type

  def initialize(key_type = :default) # :string, :default
    @cache = {}
    @key_type = key_type
  end

  def get(array)
    @cache[cache_key_for(array)]
  end

  def set(array, value)
    @cache[cache_key_for(array)] = value
  end

  private

  def cache_key_for(array)
    if @key_type == :string
      array.to_s
    elsif @key_type == :default
      array
    end
  end

end
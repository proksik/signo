class CreateDblpProcessedAuthors < ActiveRecord::Migration
  def change
    create_table :dblp_processed_authors do |t|
      t.string :name
      t.text :data
      t.timestamps
    end
    add_index :dblp_processed_authors, :name, unique: true
  end
end

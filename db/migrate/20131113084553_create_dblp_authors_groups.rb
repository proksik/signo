class CreateDblpAuthorsGroups < ActiveRecord::Migration
  def change
    create_table :dblp_authors_groups do |t|
      t.string :name
      t.text :authors
      t.timestamps
    end
  end
end

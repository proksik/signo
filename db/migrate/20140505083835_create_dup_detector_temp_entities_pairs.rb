class CreateDupDetectorTempEntitiesPairs < ActiveRecord::Migration
  def change
    create_table :dup_detector_temp_entities_pairs do |t|
      t.references :dataset, index: true
      t.references :entity, index: true
      t.references :temp_entity, index: true
      t.timestamps
    end
  end
end

class CreateGovDependOrganizations < ActiveRecord::Migration
  def change
    create_table :gov_depend_organizations do |t|
      t.integer :active_organization_id, :index
      t.integer :extinct_organization_id, :index
      t.timestamps
    end
  end
end

class AddGeneratedByToAnnotaEntitiesComponentSimilarity < ActiveRecord::Migration
  def change
    add_column :annota_entities_component_similarities, :generated_by, :string
    add_index :annota_entities_component_similarities, [:generated_by, :entity_a_id, :entity_b_id, :components], unique: true, name: 'index_annota_ent_com_sim_on_gen_entity_a_id_and_entity_b_id_com'
  end
end

class CreateAnnotaEntitiesSimilarities < ActiveRecord::Migration
  def change
    create_table :annota_entities_similarities do |t|
      t.references :entity_a, index: true
      t.references :entity_b, index: true
      t.float :sgn
      t.timestamps
    end
    add_index :annota_entities_similarities, [:entity_a_id, :entity_b_id], unique: true, name: 'index_annota_entities_sim_on_entity_a_id_and_entity_b_id'
  end
end

class CreateAnnotaEntitiesComponentSimilarities < ActiveRecord::Migration
  def change
    create_table :annota_entities_component_similarities do |t|
      t.references :entity_a, index: true
      t.references :entity_b, index: true
      t.integer :components
      t.float :sgn
      t.timestamps
    end
    add_index :annota_entities_component_similarities, [:entity_a_id, :entity_b_id, :components], unique: true, name: 'index_annota_ent_com_sim_on_entity_a_id_and_entity_b_id_com'
  end
end

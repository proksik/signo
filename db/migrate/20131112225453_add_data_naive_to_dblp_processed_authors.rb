class AddDataNaiveToDblpProcessedAuthors < ActiveRecord::Migration
  def change
    add_column :dblp_processed_authors, :data_naive, :text
    rename_column :dblp_processed_authors, :data, :data_sgn
  end
end

class CreateDblpTempAuthors < ActiveRecord::Migration
  def change
    create_table :dblp_temp_authors do |t|
      t.string :neo_id
      t.timestamps
    end
    add_index :dblp_temp_authors, :neo_id, unique: true
  end
end

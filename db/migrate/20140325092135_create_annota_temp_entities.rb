class CreateAnnotaTempEntities < ActiveRecord::Migration
  def change
    create_table :annota_temp_entities do |t|
      t.string :entity_uri
      t.references :source_entity
      t.timestamps
    end
    add_index :annota_temp_entities, :entity_uri, unique: true
    add_index :annota_temp_entities, :source_entity_id, unique: true
  end
end

class CreateDupDetectorEntities < ActiveRecord::Migration
  def change
    create_table :dup_detector_entities do |t|
      t.string :entity_uri
      t.references :dataset, index: true

      t.timestamps
    end
  end
end

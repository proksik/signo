class AddVersionToAnnotaEntities < ActiveRecord::Migration
  def change
    add_column :annota_entities, :version, :string, default: '13_11_2013'
    remove_index :annota_entities, :entity_uri
    add_index :annota_entities, [:version, :entity_uri], unique: true
    add_index :annota_entities, [:version, :entity_type]
  end
end

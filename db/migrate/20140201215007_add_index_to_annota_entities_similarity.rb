class AddIndexToAnnotaEntitiesSimilarity < ActiveRecord::Migration
  def change
    add_index :annota_entities_similarities, [:attempt, :sgn]
  end
end

class CreateDupDetectorTempEntities < ActiveRecord::Migration
  def change
    create_table :dup_detector_temp_entities do |t|
      t.string :entity_uri
      t.references :source_entity, index: true
      t.references :dataset, index: true
      t.timestamps
    end
  end
end

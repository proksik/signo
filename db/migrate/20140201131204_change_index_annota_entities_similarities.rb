class ChangeIndexAnnotaEntitiesSimilarities < ActiveRecord::Migration
  def change
    remove_index :annota_entities_similarities, name: 'index_annota_entities_sim_on_entity_a_id_and_entity_b_id'
    add_index :annota_entities_similarities, [:entity_a_id, :entity_b_id, :attempt], unique: true, name: 'index_annota_entities_sim_on_entity_a_and_entity_b_attempt'
  end
end

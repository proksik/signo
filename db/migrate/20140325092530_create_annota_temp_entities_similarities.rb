class CreateAnnotaTempEntitiesSimilarities < ActiveRecord::Migration
  def change
    create_table :annota_temp_entities_similarities do |t|
      t.references :entity, index: true
      t.references :temp_entity, index: true
      t.float :sgn
      t.timestamps
    end
    add_index :annota_temp_entities_similarities, [:entity_id, :temp_entity_id], unique: true, name: 'index_annota_temp_ent_sim_on_entity_and_temp_entity'
  end
end

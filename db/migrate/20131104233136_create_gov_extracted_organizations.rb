class CreateGovExtractedOrganizations < ActiveRecord::Migration
  def change
    create_table :gov_extracted_organizations do |t|
      t.integer :organization_id, :index
      t.timestamps
    end
  end
end

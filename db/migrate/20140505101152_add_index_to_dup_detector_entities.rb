class AddIndexToDupDetectorEntities < ActiveRecord::Migration
  def change
    add_index :dup_detector_entities, [:dataset_id, :entity_uri], unique: true
  end
end

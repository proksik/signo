class AddOriginalCountToDblpProcessedAuthors < ActiveRecord::Migration
  def change
    add_column :dblp_processed_authors, :original_count, :integer
  end
end

class CreateDupDetectorDatasets < ActiveRecord::Migration
  def change
    create_table :dup_detector_datasets do |t|
      t.string :name
      t.timestamps
    end
    add_index :dup_detector_datasets, :name, unique: true
  end
end

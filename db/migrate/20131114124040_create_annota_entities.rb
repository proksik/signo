class CreateAnnotaEntities < ActiveRecord::Migration
  def change
    create_table :annota_entities do |t|
      t.string :entity_uri
      t.string :entity_type
      t.timestamps
    end
    add_index :annota_entities, :entity_uri, unique: true
    add_index :annota_entities, :entity_type
  end
end

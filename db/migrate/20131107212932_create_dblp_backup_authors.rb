class CreateDblpBackupAuthors < ActiveRecord::Migration
  def change
    create_table :dblp_backup_authors do |t|
      t.string :name
      t.text :data
      t.timestamps
    end
    add_index :dblp_backup_authors, :name, unique: true
  end
end

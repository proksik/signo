class AddAttemptToAnnotaEntitiesSimilarity < ActiveRecord::Migration
  def change
    add_column :annota_entities_similarities, :attempt, :integer, default: 1
    add_index :annota_entities_similarities, :attempt
  end
end

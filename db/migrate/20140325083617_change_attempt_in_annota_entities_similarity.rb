class ChangeAttemptInAnnotaEntitiesSimilarity < ActiveRecord::Migration
  def up
    change_column :annota_entities_similarities, :attempt, :string
    rename_column :annota_entities_similarities, :attempt, :generated_by
    Annota::EntitiesSimilarity.where(generated_by: '2').update_all(generated_by: :nearest)
    Annota::EntitiesSimilarity.where(generated_by: '1').update_all(generated_by: :random)
  end

  def down
    Annota::EntitiesSimilarity.where(generated_by: :nearest).update_all(generated_by: '2')
    Annota::EntitiesSimilarity.where(generated_by: :random).update_all(generated_by: '1')
    rename_column :annota_entities_similarities, :generated_by, :attempt
    change_column :annota_entities_similarities, :attempt, :integer
  end

end

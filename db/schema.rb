# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140505101152) do

  create_table "annota_entities", force: true do |t|
    t.string   "entity_uri"
    t.string   "entity_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "version",     default: "13_11_2013"
  end

  add_index "annota_entities", ["entity_type"], name: "index_annota_entities_on_entity_type", using: :btree
  add_index "annota_entities", ["version", "entity_type"], name: "index_annota_entities_on_version_and_entity_type", using: :btree
  add_index "annota_entities", ["version", "entity_uri"], name: "index_annota_entities_on_version_and_entity_uri", unique: true, using: :btree

  create_table "annota_entities_component_similarities", force: true do |t|
    t.integer  "entity_a_id"
    t.integer  "entity_b_id"
    t.integer  "components"
    t.float    "sgn"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "generated_by"
  end

  add_index "annota_entities_component_similarities", ["entity_a_id", "entity_b_id", "components"], name: "index_annota_ent_com_sim_on_entity_a_id_and_entity_b_id_com", unique: true, using: :btree
  add_index "annota_entities_component_similarities", ["entity_a_id"], name: "index_annota_entities_component_similarities_on_entity_a_id", using: :btree
  add_index "annota_entities_component_similarities", ["entity_b_id"], name: "index_annota_entities_component_similarities_on_entity_b_id", using: :btree
  add_index "annota_entities_component_similarities", ["generated_by", "entity_a_id", "entity_b_id", "components"], name: "index_annota_ent_com_sim_on_gen_entity_a_id_and_entity_b_id_com", unique: true, using: :btree

  create_table "annota_entities_similarities", force: true do |t|
    t.integer  "entity_a_id"
    t.integer  "entity_b_id"
    t.float    "sgn"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "generated_by", default: "1"
  end

  add_index "annota_entities_similarities", ["entity_a_id", "entity_b_id", "generated_by"], name: "index_annota_entities_sim_on_entity_a_and_entity_b_attempt", unique: true, using: :btree
  add_index "annota_entities_similarities", ["entity_a_id"], name: "index_annota_entities_similarities_on_entity_a_id", using: :btree
  add_index "annota_entities_similarities", ["entity_b_id"], name: "index_annota_entities_similarities_on_entity_b_id", using: :btree
  add_index "annota_entities_similarities", ["generated_by", "sgn"], name: "index_annota_entities_similarities_on_generated_by_and_sgn", using: :btree
  add_index "annota_entities_similarities", ["generated_by"], name: "index_annota_entities_similarities_on_generated_by", using: :btree

  create_table "annota_temp_entities", force: true do |t|
    t.string   "entity_uri"
    t.integer  "source_entity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "annota_temp_entities", ["entity_uri"], name: "index_annota_temp_entities_on_entity_uri", unique: true, using: :btree
  add_index "annota_temp_entities", ["source_entity_id"], name: "index_annota_temp_entities_on_source_entity_id", unique: true, using: :btree

  create_table "annota_temp_entities_similarities", force: true do |t|
    t.integer  "entity_id"
    t.integer  "temp_entity_id"
    t.float    "sgn"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "annota_temp_entities_similarities", ["entity_id", "temp_entity_id"], name: "index_annota_temp_ent_sim_on_entity_and_temp_entity", unique: true, using: :btree
  add_index "annota_temp_entities_similarities", ["entity_id"], name: "index_annota_temp_entities_similarities_on_entity_id", using: :btree
  add_index "annota_temp_entities_similarities", ["temp_entity_id"], name: "index_annota_temp_entities_similarities_on_temp_entity_id", using: :btree

  create_table "dblp_authors_groups", force: true do |t|
    t.string   "name"
    t.text     "authors"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "dblp_backup_authors", force: true do |t|
    t.string   "name"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dblp_backup_authors", ["name"], name: "index_dblp_backup_authors_on_name", unique: true, using: :btree

  create_table "dblp_processed_authors", force: true do |t|
    t.string   "name"
    t.text     "data_sgn"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "data_naive"
    t.integer  "original_count"
  end

  add_index "dblp_processed_authors", ["name"], name: "index_dblp_processed_authors_on_name", unique: true, using: :btree

  create_table "dblp_temp_authors", force: true do |t|
    t.string   "neo_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dblp_temp_authors", ["neo_id"], name: "index_dblp_temp_authors_on_neo_id", unique: true, using: :btree

  create_table "dup_detector_datasets", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dup_detector_datasets", ["name"], name: "index_dup_detector_datasets_on_name", unique: true, using: :btree

  create_table "dup_detector_entities", force: true do |t|
    t.string   "entity_uri"
    t.integer  "dataset_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dup_detector_entities", ["dataset_id", "entity_uri"], name: "index_dup_detector_entities_on_dataset_id_and_entity_uri", unique: true, using: :btree
  add_index "dup_detector_entities", ["dataset_id"], name: "index_dup_detector_entities_on_dataset_id", using: :btree

  create_table "dup_detector_temp_entities", force: true do |t|
    t.string   "entity_uri"
    t.integer  "source_entity_id"
    t.integer  "dataset_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dup_detector_temp_entities", ["dataset_id"], name: "index_dup_detector_temp_entities_on_dataset_id", using: :btree
  add_index "dup_detector_temp_entities", ["source_entity_id"], name: "index_dup_detector_temp_entities_on_source_entity_id", using: :btree

  create_table "dup_detector_temp_entities_pairs", force: true do |t|
    t.integer  "dataset_id"
    t.integer  "entity_id"
    t.integer  "temp_entity_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "dup_detector_temp_entities_pairs", ["dataset_id"], name: "index_dup_detector_temp_entities_pairs_on_dataset_id", using: :btree
  add_index "dup_detector_temp_entities_pairs", ["entity_id"], name: "index_dup_detector_temp_entities_pairs_on_entity_id", using: :btree
  add_index "dup_detector_temp_entities_pairs", ["temp_entity_id"], name: "index_dup_detector_temp_entities_pairs_on_temp_entity_id", using: :btree

  create_table "gov_depend_organizations", force: true do |t|
    t.integer  "active_organization_id"
    t.integer  "index"
    t.integer  "extinct_organization_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "gov_extracted_organizations", force: true do |t|
    t.integer  "organization_id"
    t.integer  "index"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

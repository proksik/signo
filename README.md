SiGNo
==============

Konfiguracia
----------------

Aplikacia pozaduje

* rbenv (pod pouzivatelom aplikacie)
* neo4j
* postgresql

1 Stiahnut data ()

Experiment 1
----------------
exp = GOV::Experiments::Experiment1.new([1000|5000|10000|50000])
exp.train_properties # save model in data/models/gov_properties_[1000|5000|10000|50000].model
exp.train_sgn        # save model in data/models/gov_sgn_[1000|5000|10000|50000].model
exp.evaluate         # all sgn items, results to data/experiments/experiment1/[1000|5000|10000|50000]_snp_drn_nd.out
exp.evaluate(true)   # only snp, results to data/experiments/experiment1/[1000|5000|10000|50000]_snp.out
exp.evaluate_naive   # naive method, results to data/experiments/experiment1/[1000|5000|10000|50000]_naive.out

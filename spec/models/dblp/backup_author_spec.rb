require 'spec_helper'

describe DBLP::BackupAuthor do
  let(:backup_author_1) { FactoryGirl.create(:dblp_backup_author) }

  it 'should serialize hash' do
    backup_author_1
    backup_author_1.name.should == 'Maria Bielikova'
    backup_author_1.data.should == {fu: :bar}
    backup_author_1.data = {}
    backup_author_1.save
    backup_author_1.data.should == {}
  end
end

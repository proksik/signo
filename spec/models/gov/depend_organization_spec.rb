require 'spec_helper'

describe GOV::DependOrganization do
  let(:depend_organization_1) { FactoryGirl.create(:gov_depend_organization, active_organization_id: 1, extinct_organization_id: 2) }
  let(:depend_organization_2) { FactoryGirl.create(:gov_depend_organization, active_organization_id: 3, extinct_organization_id: 4) }
  let(:organization_1) { FactoryGirl.create(:gov_extracted_organization, organization_id: 1) }
  let(:organization_3) { FactoryGirl.create(:gov_extracted_organization, organization_id: 3) }

  it 'should find_active' do
    depend_organization_1
    depend_organization_2
    GOV::DependOrganization.get_active(5).should == nil
    GOV::DependOrganization.get_active(1).extinct_organization_id.should == 2
  end

  it 'should random_not_depend' do
    depend_organization_1
    depend_organization_2
    organization_1
    organization_3
    depend_organization_1.random_not_depend_id.should == depend_organization_2.active_organization_id
    depend_organization_2.random_not_depend_id.should == depend_organization_1.active_organization_id
  end

end

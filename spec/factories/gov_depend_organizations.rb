# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gov_depend_organization, :class => 'GOV::DependOrganization' do
    active_organization_id 1
    extinct_organization_id 2
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dblp_temp_author, :class => 'DBLP::TempAuthor' do
    neo_id "12345"
  end
end

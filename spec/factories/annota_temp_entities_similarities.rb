# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :annota_temp_entities_similarity, :class => 'Annota::TempEntitiesSimilarity' do
    entity nil
    temp_entity nil
    sgn 1.5
  end
end

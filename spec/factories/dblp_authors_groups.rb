# Read about factories at https://github.com/thoughtbot/factory_girl

meta_array = []

FactoryGirl.define do
  factory :dblp_authors_group, :class => 'DBLP::AuthorsGroup' do
    name 'group'
    authors meta_array
  end
end

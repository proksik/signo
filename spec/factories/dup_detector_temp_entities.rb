# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dup_detector_temp_entity, :class => 'DupDetector::TempEntity' do
    entity_uri "MyString"
    source_entity nil
  end
end

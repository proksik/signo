# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :annota_entity, :class => 'Annota::Entity' do
    uri 'organization_1'
    type 'organization'
  end
end

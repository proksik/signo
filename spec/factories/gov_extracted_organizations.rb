# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :gov_extracted_organization, :class => 'GOV::ExtractedOrganization' do
    organization_id 1
  end
end

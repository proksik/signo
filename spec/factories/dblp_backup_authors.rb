# Read about factories at https://github.com/thoughtbot/factory_girl

meta_hash = {fu: :bar}

FactoryGirl.define do
  factory :dblp_backup_author, :class => 'DBLP::BackupAuthor' do
    name 'Maria Bielikova'
    data meta_hash
  end
end

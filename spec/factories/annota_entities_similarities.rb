# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :annota_entities_similarity, :class => 'Annota::EntitiesSimilarity' do
    entity_a nil
    entity_b nil
    sgn 1.5
  end
end

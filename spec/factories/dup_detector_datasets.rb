# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dup_detector_dataset, :class => 'DupDetector::Dataset' do
    name "MyString"
  end
end

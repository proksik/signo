# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :annota_temp_entity, :class => 'Annota::TempEntity' do
    entity_uri "MyString"
    source_entity nil
  end
end

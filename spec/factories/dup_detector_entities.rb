# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dup_detector_entity, :class => 'DupDetector::Entity' do
    entity_uri "MyString"
    dataset nil
  end
end

# Read about factories at https://github.com/thoughtbot/factory_girl

meta_hash = {in_proceedings: [], in_collections: [], articles: []}

FactoryGirl.define do
  factory :dblp_processed_author, :class => 'DBLP::ProcessedAuthor' do
    name 'Maria Bielikova'
    data meta_hash
  end
end

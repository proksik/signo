# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :dup_detector_temp_entities_pair, :class => 'DupDetector::TempEntitiesPair' do
    entity nil
    temp_entity nil
  end
end

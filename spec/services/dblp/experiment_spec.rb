# encoding: utf-8

require 'spec_helper'

describe DBLP::Experiments::Experiment do

  it 'should merge nodes' do
    pending 'Clear neo4j'

    neo4j_clear_and_restart
    exp = DBLP::Experiments::Experiment.new

    node_1 = Neography::Node.create(name: 'test1', value: 'value1')
    node_2 = Neography::Node.create(name: 'test2', value: 'value2', name2: 'name2')
    node_3 = Neography::Node.create(name: 'test3', value: 'value3')
    node_1.incoming(:rel) << node_3
    node_2.outgoing(:rel) << node_3
    node_2.incoming(:rel2) << node_3

    merged_node = exp.merge_nodes(node_1, node_2)
    merged_node[:name].should == 'test1'
    merged_node[:value].should == 'value1'
    merged_node[:name2].should == 'name2'
    merged_node.incoming(:rel).map { |n| n }[0].should == node_3
    merged_node.outgoing(:rel).map { |n| n }[0].should == node_3
    merged_node.incoming(:rel2).map { |n| n }[0].should == node_3
  end

  it 'should backup and restore node 1' do
    pending 'Clear neo4j'

    neo4j_clear_and_restart
    exp = DBLP::Experiments::Experiment.new

    node_1 = Neography::Node.create(name: 'test1', value: 'value1')
    node_2 = Neography::Node.create(name: 'test2', value: 'value2')
    node_1.both(:rel) << node_2

    backup_node_1 = exp.backup_node(node_1)

    neo = Neography::Rest.new
    neo.delete_node!(node_1.neo_id)

    restored_node = exp.restore_node(backup_node_1)
    restored_node.name.should == 'test1'
    rels = restored_node.rels.map { |r| r }
    rels[0].rel_type.should == 'rel'
    rels[0].start_node.should == restored_node
    rels[0].end_node.should == node_2
    rels[1].rel_type.should == 'rel'
    rels[1].end_node.should == restored_node
    rels[1].start_node.should == node_2
  end

  it 'should backup and restore node 2' do
    pending 'Clear neo4j'

    neo4j_clear_and_restart
    exp = DBLP::Experiments::Experiment.new

    node_1 = Neography::Node.create(name: 'test1', value: 'value1')
    node_2 = Neography::Node.create(name: 'test2', value: 'value2')
    node_3 = Neography::Node.create(name: 'test3', value: 'value3')
    node_1.incoming(:rel1) << node_2
    node_1.outgoing(:rel2) << node_3

    backup_node_1 = exp.backup_node(node_1)

    neo = Neography::Rest.new
    neo.delete_node!(node_1.neo_id)

    restored_node = exp.restore_node(backup_node_1)
    restored_node.name.should == 'test1'
    rels = restored_node.rels.map { |r| r }
    rels[1].rel_type.should == 'rel2'
    rels[1].start_node.should == restored_node
    rels[1].end_node.should == node_3
    rels[0].rel_type.should == 'rel1'
    rels[0].end_node.should == restored_node
    rels[0].start_node.should == node_2
  end

  it 'should backup and restore author - using db' do
    pending 'Clear neo4j'

    neo4j_clear_and_restart
    exp = DBLP::Experiments::Experiment.new

    node_1 = Neography::Node.create(name: 'test1', value: 'value1')
    node_2 = Neography::Node.create(name: 'test2', value: 'value2')
    node_3 = Neography::Node.create(name: 'test3', value: 'value3')
    node_1.incoming(:rel1) << node_2
    node_1.outgoing(:rel2) << node_3

    # backup to db
    exp.backup_author_node(node_1)

    neo = Neography::Rest.new
    neo.delete_node!(node_1.neo_id)

    # restore from db
    restored_node = exp.restore_author_node(node_1[:name])

    restored_node.name.should == 'test1'
    rels = restored_node.rels.map { |r| r }
    rels[1].rel_type.should == 'rel2'
    rels[1].start_node.should == restored_node
    rels[1].end_node.should == node_3
    rels[0].rel_type.should == 'rel1'
    rels[0].end_node.should == restored_node
    rels[0].start_node.should == node_2

    author = DBLP::Author.find('test1')
    author.should_not == nil
    author[:value] == 'value1'
  end

  it 'should create and delete temp author' do
    pending 'Clear neo4j'
    exp = DBLP::Experiments::Experiment.new

    DBLP::TempAuthor.count.should == 0

    author_node_1 = exp.create_temp_author('author name 1')
    author_node_2 = exp.create_temp_author('author name 2')

    DBLP::TempAuthor.count.should == 2

    exp.delete_temp_author(author_node_1)
    exp.delete_temp_author(author_node_2)

    DBLP::TempAuthor.count.should == 0

  end

  it 'should merge temp authors' do
    pending 'Clear neo4j'

    neo4j_clear_and_restart
    exp = DBLP::Experiments::Experiment.new

    author_node_1 = exp.create_temp_author('author name 1')
    author_node_2 = exp.create_temp_author('author name 2')
    author_node_3 = exp.create_temp_author('author name 3')
    author_node_1.incoming(:rel) << author_node_3
    author_node_2.outgoing(:rel) << author_node_3
    author_node_2.incoming(:rel2) << author_node_3

    DBLP::TempAuthor.count.should == 3

    merged_node = exp.merge_temp_authors(author_node_1, author_node_2)
    merged_node[:name].should == 'author name 1'
    merged_node.incoming(:rel).map { |n| n }[0].should == author_node_3
    merged_node.outgoing(:rel).map { |n| n }[0].should == author_node_3
    merged_node.incoming(:rel2).map { |n| n }[0].should == author_node_3

    DBLP::TempAuthor.count.should == 2
  end

end
# encoding: utf-8

require 'spec_helper'

describe DBLP::ParsingAndExtracting::Service do

  it 'should parse pewe people' do
    serv = DBLP::ParsingAndExtracting::Service.new
    people, last_year = serv.parse_pewe_people
    last_year.should == '2013/14'
    people[last_year].count.should == 52

  end

end
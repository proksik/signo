# encoding: utf-8

require 'spec_helper'

describe SGN::GOV::NoTrainable::Service do

  it 'should find similarity organizations' do
    s = SGN::GOV::NoTrainable::Service.new
    # sim organizations from one district, one place, one ico, but other sk_nace
    sim_1 = s.similarity_of_organizations(1118957, 7391)
    sim_1.should > 0.85

    # not sim organizations from one district, one place, other sk_nace, other ico and name
    sim_2 = s.similarity_of_organizations(1104951, 7391)
    sim_2.should < 0.65

    # sim organizations from one district, one place, one not have sk_nace, same ico
    sim_3 = s.similarity_of_organizations(1112225, 12252)
    sim_3.should > 0.6

    # sim organizations from other all
    sim_4 = s.similarity_of_organizations(1266282, 1253287)
    sim_4.should < 0.35

    # sim organizations - same
    sim_5 = s.similarity_of_organizations(1118957, 1118957)
    sim_5.should == 1.0
  end

  it 'should find sgn' do
    s = SGN::GOV::NoTrainable::Service.new
    # sim places - Bratislava and Male Zaluzie
    sim_1 = s.sgn(GOV::Place.find(469), GOV::Place.find(1141))
    sim_1.should < 0.41
  end

end
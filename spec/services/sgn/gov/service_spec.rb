# encoding: utf-8

require 'spec_helper'

describe SGN::GOV::NoTrainable do


  it 'should find sgn' do
    s = SGN::GOV::NoTrainable.new(nil, nil)
    sim = s.similarity_of_organizations(1118957, 7391)
    sim.should == nil
  end

end
# encoding: utf-8

require 'spec_helper'

describe SGN::Core::Service do

  it 'should calculate number similarity' do
    s = SGN::Core::Service.new
    sim_1 = s.number_similarity(1, 10, 10)
    sim_1.should < 0.15
    sim_2 = s.number_similarity(1, 2, 1000)
    sim_2.should > 0.9
    sim_3 = s.number_similarity(-1, 20, 10)
    sim_3.should < 0.1
  end

  it 'should find distance from node_1 to node_2' do
    s = SGN::Core::Service.new
    # from BB region to Brezon district
    dist = s.nodes_distance(GOV::Region.find(1), GOV::District.find(3))
    dist.should == 1.0
    # from Male Zaluzie place to Nitra region
    dist = s.nodes_distance(GOV::Region.find(4), GOV::Place.find(1141))
    dist.should == 2.0
    # from Bratislava place to Male Zaluzie place
    dist = s.nodes_distance(GOV::Place.find(469), GOV::Place.find(1141))
    dist.should == 5.0
  end

end
# encoding: utf-8

require 'spec_helper'

describe GOV::Extracting::Service do

  it 'should extract dependence organizations' do
    s = GOV::Extracting::Service.new(false)
    s.extract_depend_organizations(100)
    GOV::DependOrganization.count.should == 100
    GOV::DependOrganization.first.active_organization_id.should == 1307842
    GOV::DependOrganization.first.extinct_organization_id.should == 584376
    GOV::DependOrganization.last.active_organization_id.should == 1236470
    GOV::DependOrganization.last.extinct_organization_id.should == 716392
  end

  it 'should extract regions and regions relations' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    s.extract_regions
    neo4j_count_nodes(:region).should == 8
    regions = neo4j_nodes(:region)
    regions.first[:name].should == 'Žilinský kraj'
    regions.last[:name].should == 'Trnavský kraj'
    regions.last[:office_address].should == 'Kollárova 8'

    s.extract_regions_relations
    regions[0][:name].should == 'Žilinský kraj'
    regions[4][:name].should == 'Trenčianský kraj'
    regions[0].outgoing(:neighbour).any? { |region| region.neo_id == regions[4].neo_id }.should == true
    regions[4].outgoing(:neighbour).any? { |region| region.neo_id == regions[0].neo_id }.should == true
  end

  it 'should extract districts and districts relations' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    # because district is  related to region
    s.extract_regions
    regions = neo4j_nodes(:region)
    s.extract_districts
    neo4j_count_nodes(:district).should == 79
    districts = neo4j_nodes(:district)
    districts[0][:name].should == 'Sabinov'
    districts.last[:name].should == 'Trebišov'
    districts.last[:count_active_organizations].should == 8534
    districts.last[:region_name].should == 'Košický kraj'

    s.extract_districts_relations
    districts[0][:name].should == 'Sabinov'
    districts[62][:name].should == 'Banská Bystrica'
    districts[1].outgoing(:neighbour).any? { |district| district.neo_id == districts[62].neo_id }.should == true
    districts[62].outgoing(:neighbour).any? { |district| district.neo_id == districts[1].neo_id }.should == true

    regions[3][:name].should == 'Košický kraj'
    regions[3].outgoing(:districts).any? { |district| district.neo_id == districts.last.neo_id }.should == true
    districts.last.incoming(:districts).first.should == regions[3]
  end

  it 'should extract places' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    # because district is  related to regions
    s.extract_regions
    regions = neo4j_nodes(:region)
    # because places is related to districts
    s.extract_districts
    districts = neo4j_nodes(:district)

    s.extract_places(10)
    places = neo4j_nodes(:place)
    neo4j_count_nodes(:place).should == 10

    places.first[:name].should == 'Drahovce'
    places.first[:count_active_organizations].should == 261
    places.first[:district_name].should == 'Piešťany'
    places.last[:district_name].should == 'Senica'
    places.last[:is_city].should == false

    districts[33].outgoing(:places).any? { |place| place.neo_id == places.first.neo_id }.should == true
    places.first.incoming(:places).first.should == districts[33]
  end

  it 'should extract places and places_relations' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    # because district is  related to regions
    s.extract_regions
    # because places is related to districts
    s.extract_districts
    s.extract_places
    places = neo4j_nodes(:place)
    neo4j_count_nodes(:place).should == 2930
    s.extract_places_relations(10)
    places[528].outgoing(:neighbour).any? { |place| place.neo_id == places[2372].neo_id }.should == true
  end

  it 'should extract sk_nace_codes' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    s.extract_sk_nace_codes
    sk_nace_codes = neo4j_nodes(:sk_nace_code)
    neo4j_count_nodes(:sk_nace_code).should == 646
    sk_nace_codes.first[:name].should == 'Pestovanie nápojových plodín'
    sk_nace_codes.last[:name].should == 'Služby týkajúce sa telesnej pohody'
  end

  it 'should extract organizations' do
    pending 'Extracting data with clear neo4j'

    # db clear
    neo4j_clear_and_restart

    s = GOV::Extracting::Service.new
    s.extract_regions
    s.extract_districts
    s.extract_places
    s.extract_organizations('organizations1000', 10)
    organizations = neo4j_nodes(:organization)
    neo4j_count_nodes(:organization).should == 10
    GOV::ExtractedOrganization.count.should == 10
    organizations.first[:name].should == 'Slavomír Dulin'
    organizations.first[:ico].should == '43466257'
    organizations.first[:address].should == 'Matice slovenskej 606/18,  083 01 Sabinov'
  end

end
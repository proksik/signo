module FixtureHelpers
  def fixture(path)
    Rails.root.join('spec', 'fixtures', path).to_s
  end

  def open_fixture(path)
    File.open(fixture(path))
  end

  def read_fixture(path)
    File.read(fixture(path))
  end

  def save_fixture(path, data)
    File.open(fixture(path), 'w') { |f| f.write(data) }
  end

  def file_upload_from_fixture(path, mime_type, binary)
    fixture_file_upload(path, mime_type, binary)
  end
end

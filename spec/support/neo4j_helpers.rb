module Neo4jHelpers

  def neo4j_count_nodes(type=nil)
    neo = Neography::Rest.new
    q = if type
          neo.execute_query("START n=node:types_index(type='#{type.to_s}') RETURN COUNT(n)")
        else
          neo.execute_query('START n=node(*) RETURN COUNT(n)')
        end
    q['data'][0][0].to_i
  end

  def neo4j_nodes(type=nil)
    neo = Neography::Rest.new
    q = if type
          neo.execute_query("START n=node:types_index(type='#{type.to_s}') RETURN n")
        else
          neo.execute_query('START n=node(*) RETURN n')
          end
    q['data'].map { |data| Neography::Node.load data[0]['self'] }
  end

  def neo4j_count_relationships
    neo = Neography::Rest.new
    q = neo.execute_query('START n=relationship(*) RETURN COUNT(n)')
    q['data'][0][0].to_i
  end

  def neo4j_node_with_neo_id(neo_id)
    Neography::Node.load(neo_id)
  end

  def neo4j_clear_and_restart
    puts 'Restarting ...'
    system('/opt/neo4j/neo4j-1.9.4/bin/clear.sh')
    puts 'Restarted'
  end

end

require 'enumerable/combinatorics'

class Annota::EntitiesComponentSimilarity < ActiveRecord::Base
  bitmask :components, :as => [:snp, :drn, :nd, :srn]

  belongs_to :entity_a, class_name: 'Annota::Entity'
  belongs_to :entity_b, class_name: 'Annota::Entity'

  scope :generated_by, ->(generated_by){ where(generated_by: generated_by) }
  scope :by_components, ->(components){ where(components: components_numeric(components) ) }

  def self.components_combinations
    bitmask_definitions[:components].values.combinations
  end

  def self.components_numeric(components)
    new(components: components).components.to_i
  end

end

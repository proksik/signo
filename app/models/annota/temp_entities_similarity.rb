class Annota::TempEntitiesSimilarity < ActiveRecord::Base
  belongs_to :entity, class_name: 'Annota::Entity'
  belongs_to :temp_entity, class_name: 'Annota::TempEntity'
end

require 'neo4j_signo_plugin'

class Annota::Entity < ActiveRecord::Base
  include LOD::Includes::Common
  has_many :annota_entities_similarities, :class_name => 'Annota::EntitiesSimilarity', foreign_key: :entity_a_id
  has_many :annota_temp_entities, :class_name => 'Annota::TempEntity', foreign_key: :source_entity_id
  has_one :annota_temp_entity, :class_name => 'Annota::TempEntity', foreign_key: :source_entity_id

  scope :from_version, ->(version){ where(version: version) }

  def node
    find_or_create_in_neo4j self.entity_uri
  end

  def remaining_random_entities(total_count,generated_by=nil)
    actual_authors = annota_entities_similarities.where(generated_by: generated_by).map { |es| es.entity_b_id }
    actual_count = actual_authors.count
    actual_authors << self.id
    return [] if actual_count > total_count
    Annota::Entity.from_version(self.version).where("id NOT IN (?)", actual_authors).order("RANDOM()").limit(total_count - actual_count)
  end

  def nearest_entities(total_count)
    k = 1
    while true
      entities = Annota::Entity.from_version(self.version).where("entity_uri IN (?) AND entity_type = ?", Neo4jSignoPlugin.k_nearest_nodes(node, k*total_count).map{|n| n.uri}, 'author').limit(5)
      return entities if entities.count == 5
      k+= 1
    end
    []
  end

end

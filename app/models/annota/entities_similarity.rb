class Annota::EntitiesSimilarity < ActiveRecord::Base
  belongs_to :entity_a, class_name: 'Annota::Entity'
  belongs_to :entity_b, class_name: 'Annota::Entity'

  scope :generated_by, ->(generated_by){ where(generated_by: generated_by) }
end

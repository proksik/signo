module Shared
  module Neo4jHelper

    # merge properties node 2 to node 1, ignore same properties
    def merge_nodes(node_1, node_2)
      properties_2 = node_properties(node_2)
      properties_2.each_pair do |property, value|
        node_1[property] = value unless node_1.attributes.include? property
      end
      node_2.rels.each do |rel|
        if rel.start_node == node_2 && !rel.end_node.eql?(node_1)
          node_1.outgoing(rel.rel_type) << rel.end_node
        elsif !rel.start_node.eql?(node_1)
          node_1.incoming(rel.rel_type) << rel.start_node
        end
      end
      # delete node with relationships
      neo = Neography::Rest.new
      neo.delete_node!(node_2.neo_id)
      node_1
    end

    # restore node from backup node
    def restore_node(backup_node)
      n = Neography::Node.create(backup_node[:prop])
      backup_node[:rels].each do |rel|
        rel_type = rel[:rel_type]
        if rel[:start_node_id].nil?
          n.outgoing(rel_type) << Neography::Node.load(rel[:end_node_id])
        else
          n.incoming(rel_type) << Neography::Node.load(rel[:start_node_id])
        end
      end
      n
    end

    # create backup node from node
    def backup_node(node)
      {node_id: node.neo_id,
       prop: node_properties(node),
       rels: node.rels.map { |r|
         {start_node_id: (node == r.start_node) ? nil : r.start_node.neo_id, end_node_id: (node == r.end_node) ? nil : r.end_node.neo_id, rel_type: r.rel_type}
       }}
    end

    def outgoing_rels(node)
      node.rels.map { |r|
        temp_node = if node == r.start_node
                 r.end_node
               else
                 r.start_node
               end
        {node: temp_node, rel_type: r.rel_type} }
    end

    def grouped_outgoing_rels(node_1)
      rels = {}
      outgoing_rels(node_1).each do |rel|
        rels[ rel[:rel_type] ] = [] unless rels.has_key? rel[:rel_type]
        rels[ rel[:rel_type] ] << rel[:node]
      end
      rels
    end  

    def print_node(backup)
      rels = backup[:rels]
      rels.each do |rel|
        puts "#{rel[:start_node_id]} #{rel[:end_node_id]} #{rel[:rel_type]}"
      end
      puts backup[:prop].inspect
    end

    # get node properties as hash
    def node_properties(node)
      Hash[node.attributes.map { |a| [a, node[a]] }]
    end

    def delete_node!(node)
      neo = Neography::Rest.new
      neo.delete_node!(node.neo_id)
    end

  end
end
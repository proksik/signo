class DBLP::Year < Neography::Node
  extend Shared::DBLPHelper

  def self.create(properties)
    Neography::Node.create({type: :year}.merge(properties))
  end

  def self.find_or_create(year)
    return nil unless year
    y = Neography::Node.find('years_index', :year, year)
    unless y
      y = create(year: year)
      y.add_to_index('years_index', :year, year)
      y.add_to_index('types_index', :type, :year)
    end
    y
  end
end
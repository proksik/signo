class DBLP::AuthorsGroup < ActiveRecord::Base
  serialize :authors

  def self.update_group(group_name, authors)
    group = where(name: group_name).first_or_initialize
    group.authors = authors
    group.save
  end

end

class DBLP::ProcessedAuthor < ActiveRecord::Base
  extend DBLP::Includes::Common

  serialize :data_sgn
  serialize :data_naive

  scope :unprocessed, :conditions => "original_count IS NULL"
  scope :processed, :conditions => "original_count IS NOT NULL"

  def self.next_in_group
    where(original_count: nil).order("RANDOM()").first
  end

  def self.find_or_create(name)
    name = transform_to_index(name)
    processed = where(name: name).first
    if processed.nil? || processed.data_sgn.nil? || processed.data_naive.nil?
      exp = DBLP::Experiments::Experiment.new
      exp.process_author(name)
    end
    where(name: name).first
  end

end

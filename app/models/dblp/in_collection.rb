# incollection - 25404 items - ["mdate", "key", "author", "title", "pages", "year", "crossref", "booktitle", "url", "ee"]

class DBLP::InCollection < Neography::Node
  extend Shared::DBLPHelper

  def self.create(properties)
    Neography::Node.create({type: :in_collection}.merge(properties))
  end

  def self.extract(item)
    title = parse_title(parse_item(item['title']))

    in_collection = create(title: title, pages: parse_item(item['pages']), crossref: parse_item(item['crossref']), url: parse_item(item['url']), ee: parse_item(item['ee']))
    in_collection.add_to_index('types_index', :type, :in_collection)

    # author
    if item['author']
      item['author'].each do |author_name|
        author = DBLP::Author.find_or_create(author_name)
        author.outgoing(:in_collections) << in_collection
        #in_collection.outgoing(:authors) << author
      end
    end
    #year
    year = DBLP::Year.find_or_create(parse_year(item['year']))
    if year
      in_collection.outgoing(:year) << year
      #year.outgoing(:in_collections) << in_collection
    end

    # book
    book = DBLP::Book.find_or_create(parse_str(parse_item(item['booktitle'])))
    if book
      in_collection.outgoing(:book) << book
      #book.outgoing(:in_collections) << in_collection
    end
  end

  def self.year(node)
    node.outgoing(:year).first[:year]
  end

end
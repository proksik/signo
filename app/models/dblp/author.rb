class DBLP::Author < Neography::Node
  extend Shared::DBLPHelper

  AUTHORS_INDEX = 'authors_index'
  TYPES_INDEX = 'types_index'

  def self.url
    self['self']
  end

  def self.to_friendly_url(name)
    name.gsub(' ', '_')
  end

  def self.from_friendly_url(name)
    find reverse_name name.gsub('_', ' ')
  end

  def self.find(author_name)
    puts author_name
    Neography::Node.find(AUTHORS_INDEX, :name, transform_to_index(author_name))
  end

  def self.create(properties)
    Neography::Node.create({type: :author}.merge(properties))
  end

  def self.find_or_create(name)
    author = find(name)
    unless author
      author = create(name: name)
      author.add_to_index(AUTHORS_INDEX, :name, transform_to_index(name))
      author.add_to_index(TYPES_INDEX, :type, :author)
    end
    author
  end

  def self.find_name(name)
    find(name) || ::DBLP::Author.find(reverse_name(name))
  end
end
class GOV::Organization < Neography::Node

  TYPE = :organization
  INDEX = 'organization_index'
  INDEX_TYPE = 'types_index'
  IGNORE_FOR_PROPERTIES = [:id, :sk_nace_code_id, :district_id, :place_id]

  def self.create(data)
    properties = data.select { |k, _| !IGNORE_FOR_PROPERTIES.include? k }.merge({type: TYPE})
    organization = super(properties)
    organization.add_to_index(INDEX, :id, data[:id])
    organization.add_to_index(INDEX_TYPE, :type, TYPE)
    district = GOV::District.find(data[:district_id])
    district.outgoing(:organizations) << organization
    place = GOV::Place.find(data[:place_id])
    place.outgoing(:organizations) << organization
    if data[:sk_nace_code_id]
      sk_nace_code = GOV::SkNaceCode.find(data[:sk_nace_code_id])
      sk_nace_code.outgoing(:organizations) << organization
    end
  end

  def self.find(organization_id)
    org = Neography::Node.find(INDEX, :id, organization_id)
    return org.first if org.is_a? Array
    org
  end

end
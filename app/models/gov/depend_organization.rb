class GOV::DependOrganization < ActiveRecord::Base

  belongs_to :active_organization, class_name: GOV::ExtractedOrganization
  belongs_to :extinct_organization, class_name: GOV::ExtractedOrganization

  def self.get_active(active_org_id)
    where(active_organization_id: active_org_id).first
  end

  def random_not_depend_id
    GOV::ExtractedOrganization.where('organization_id != ? AND organization_id != ?', self.extinct_organization_id, self.active_organization_id).order('RANDOM()').first.organization_id
  end

end

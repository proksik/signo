class GOV::SkNaceCode < Neography::Node

  TYPE = :sk_nace_code
  INDEX = 'sk_nace_codes_index'
  INDEX_TYPE = 'types_index'
  IGNORE_FOR_PROPERTIES = [:id]

  def self.create(data)
    properties = data.select { |k, _| !IGNORE_FOR_PROPERTIES.include? k }.merge({type: TYPE})
    sk_nace_code = super(properties)
    sk_nace_code.add_to_index(INDEX, :id, data[:id])
    sk_nace_code.add_to_index(INDEX_TYPE, :type, TYPE)
  end

  def self.find(sk_nace_code_id)
    Neography::Node.find(INDEX, :id, sk_nace_code_id)
  end

end
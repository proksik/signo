class GOV::Region < Neography::Node

  TYPE = :region
  INDEX = 'regions_index'
  INDEX_TYPE = 'types_index'
  IGNORE_FOR_PROPERTIES = [:id]

  def self.create(data)
    properties = data.select { |k, _| !IGNORE_FOR_PROPERTIES.include? k }.merge({type: TYPE})
    region = super(properties)
    region.add_to_index(INDEX, :id, data[:id])
    region.add_to_index(INDEX_TYPE, :type, TYPE)
  end

  def self.create_relation(region_a_id, region_b_id)
    region_a = find(region_a_id)
    region_b = find(region_b_id)
    region_a.outgoing(:neighbour) << region_b
  end

  def self.find(region_id)
    Neography::Node.find('regions_index', :id, region_id)
  end

end
class GOV::Place < Neography::Node

  TYPE = :place
  INDEX = 'places_index'
  INDEX_TYPE = 'types_index'
  IGNORE_FOR_PROPERTIES = [:id, :district_id]

  def self.create(data)
    properties = data.select { |k, _| !IGNORE_FOR_PROPERTIES.include? k }.merge({type: TYPE})
    place = super(properties)
    place.add_to_index(INDEX, :id, data[:id])
    place.add_to_index(INDEX_TYPE, :type, TYPE)
    district = GOV::District.find(data[:district_id])
    district.outgoing(:places) << place
  end

  def self.create_relation(place_a_id, place_b_id)
    place_a = find(place_a_id)
    place_b = find(place_b_id)
    place_a.outgoing(:neighbour) << place_b
  end

  def self.find(place_id)
    Neography::Node.find(INDEX, :id, place_id)
  end

end
class GOV::District < Neography::Node

  TYPE = :district
  INDEX = 'districts_index'
  INDEX_TYPE = 'types_index'
  IGNORE_FOR_PROPERTIES = [:id, :region_id]

  def self.create(data)
    properties = data.select { |k, _| !IGNORE_FOR_PROPERTIES.include? k }.merge({type: TYPE})
    district = super(properties)
    district.add_to_index(INDEX, :id, data[:id])
    district.add_to_index(INDEX_TYPE, :type, TYPE)
    region = GOV::Region.find(data[:region_id])
    region.outgoing(:districts) << district
  end

  def self.create_relation(district_a_id, district_b_id)
    district_a = find(district_a_id)
    district_b = find(district_b_id)
    district_a.outgoing(:neighbour) << district_b
  end

  def self.find(district_id)
    Neography::Node.find(INDEX, :id, district_id)
  end

end
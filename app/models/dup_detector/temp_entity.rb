class DupDetector::TempEntity < ActiveRecord::Base
  include LOD::Includes::Common

  belongs_to :source_entity, class_name: DupDetector::Entity

  def node
    find_in_neo4j self.entity_uri
  end

end

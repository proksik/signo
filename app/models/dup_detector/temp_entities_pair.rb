class DupDetector::TempEntitiesPair < ActiveRecord::Base
  belongs_to :dataset, class_name: DupDetector::Dataset
  belongs_to :entity, class_name: DupDetector::Entity
  belongs_to :temp_entity, class_name: DupDetector::TempEntity

end

class DupDetector::Entity < ActiveRecord::Base
  include LOD::Includes::Common

  belongs_to :dataset, class_name: DupDetector::Dataset
  has_many :temp_entities, :class_name => DupDetector::TempEntity, foreign_key: :source_entity_id
  has_one :temp_entity, :class_name => DupDetector::TempEntity, foreign_key: :source_entity_id

  def node
    find_or_create_in_neo4j self.entity_uri
  end

end

class DupDetector::Dataset < ActiveRecord::Base
  has_many :entities, :class_name => DupDetector::Entity, foreign_key: :dataset_id
  has_many :temp_entities, :class_name => DupDetector::TempEntity, foreign_key: :dataset_id
  has_many :temp_entities_pairs, :class_name => DupDetector::TempEntitiesPair, foreign_key: :dataset_id
end

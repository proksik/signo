module ApplicationHelper
  def active_class(actual_value, active_value)
    (actual_value == active_value) ? 'active' : nil
  end
end

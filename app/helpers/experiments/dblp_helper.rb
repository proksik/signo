module Experiments::DblpHelper

  def format_authors(authors)
    authors.map { |author| author[:name] }.join(', ')
  end

  def reverse_name(name)
    name.split(' ').reverse.join(' ')
  end

end

require 'progress_bar'
require 'correl_coeff'
require 'svm'

module DupDetector
  class Evaluation
    include LOD::Includes::Common
    include DupDetector::Includes::Preprocessing

    attr_accessor :dataset, :test_properties, :correlation_coefficients

    def initialize(dataset_name)
      @dataset = DupDetector::Dataset.find_or_create_by(name: dataset_name)
      @correlation_coefficients = current_coefficients
    end

    def current_coefficients
      cc = CorrelCoeff.new correl_coeff_model_name
      return cc.coefficients if cc.exists?
      {}
    end

    def find_duplicates(file_name)
      pairs = load_pairs file_name
      entities_urls = (pairs.map{|p| p.first} +  pairs.map{|p| p.last}).uniq
      entities = {}
      entities_urls.each do |entity_uri|
        entities[entity_uri] = find_in_neo4j entity_uri
      end

      test_properties = properties_from entities.values
      sgn_dd = SGN::DupDetector.new test_properties, @correlation_coefficients, true, svm_model_name('sgn')

      # progress bar
      progress_bar = ProgressBar.new pairs.count

      duplicates = []
      pairs.each do |entity_uri_a, entity_uri_b|
        entity_a = entities[entity_uri_a]
        entity_b = entities[entity_uri_b]
        if sgn_dd.similar?(entity_a, entity_b)
          duplicates << [entity_uri_a, entity_uri_b]
          puts "Duplicate: #{entity_uri_a} #{entity_uri_b}"
        end

        # progressbar increment
        progress_bar.increment
      end

      save_duplicates(file_name, duplicates)
    end

    def save_duplicates(file_name, duplicates)
      f = File.open(duplicate_file_path(file_name + '_duplicates'), 'w')
      duplicates.each do |entity_uri_a, entity_uri_b|
        f.write "#{entity_uri_a} #{entity_uri_b}\n"
      end
      f.close
    end

    def load_pairs(file_name)
      f = File.open(duplicate_file_path(file_name))
      lines = f.read.split("\n")
      f.close
      pairs = []
      lines.each do |line|
        urls = line.split(' ')
        pairs << urls
      end
      pairs
    end

    private

    def svm_model_name(type)
      "dup_detector_#{@dataset.name}_#{type}"
    end

    def correl_coeff_model_name
      "dup_detector_#{@dataset.name}"
    end

    def duplicate_file_path(file_name)
      "#{DATA_PATH}#{@dataset.name}/#{file_name}"
    end

    DATA_PATH = 'data/dup_detector/'

  end
end

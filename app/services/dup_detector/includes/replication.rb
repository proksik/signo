module DupDetector
  module Includes
    module Replication
      include Shared::Neo4jHelper

      def destroy_temp_nodes(dataset)
        dataset.temp_entities.each do |temp_entity|
          delete_node! temp_entity.node if temp_entity.node
          temp_entity.destroy!
        end
      end

      COUNT_REPLICATES = 250

      def generate_temp_entities(dataset, count_replicates = COUNT_REPLICATES,replication_prefix = nil)
        destroy_temp_nodes(dataset)
        if replication_prefix
          source_entities = dataset.entities.where("entity_uri LIKE '#{replication_prefix}%'")
        else
          source_entities = dataset.entities
        end
        source_entities.order('RANDOM()').limit(count_replicates).each do |source_entity|
          replicate_entity(dataset, source_entity)
        end
        nil
      end

      def generate_pairs(dataset)
        dataset.temp_entities_pairs.delete_all
        dataset.temp_entities.each do |temp_entity|
          source_entity = temp_entity.source_entity
          dataset.temp_entities_pairs.create(temp_entity_id: temp_entity.id, entity_id: source_entity.id)
          10.times do
            source_entity_2 = dataset.temp_entities.order('RANDOM()').first.source_entity
            dataset.temp_entities_pairs.create(temp_entity_id: temp_entity.id, entity_id: source_entity_2.id) unless dataset.temp_entities_pairs.exists?(temp_entity_id: temp_entity.id, entity_id: source_entity_2.id)
          end
        end
      end

      def replicate_entity(dataset, source_entity)
        entity_uri = temp_entity_uri(source_entity)
        temp_entity = source_entity.temp_entities.first_or_initialize entity_uri: entity_uri, dataset_id: dataset.id
        replicate_node(dataset, entity_uri, source_entity.node)
        temp_entity.save
      end

      def replicate_node(dataset, entity_uri, source_node)
        new_node = find_or_create_in_neo4j(entity_uri)

        source_properties = node_properties(source_node)
        source_properties.each_pair do |attribute_name, value|
          if attribute_name != :uri
            if random_bool
              new_node[attribute_name] = remake_value value
            else
              new_node[attribute_name] = value
            end
          end
        end

        outgoing_rels(source_node).each do |rel|
          if random_bool
            new_node.outgoing(rel[:rel_type]) << rel[:node]
          else
            new_node.outgoing(rel[:rel_type]) << random_node(dataset)
          end
        end

        new_node.save
      end

      private

      def temp_entity_uri(source_entity)
        "<temp_entity_from_#{source_entity.id}>"
      end

      def random_bool
        rand(10).zero?
      end

      def remake_value(value)
        if value.is_a? Integer
          value + rand(10)
        else
          value.to_s.each_char.map { |c|
            if random_bool
              a = ((c.ord + rand(10)) % 230).chr
              a.force_encoding('UTF-8')
            else
              c
            end
          }.join
        end
      end

      def random_node(version)
        entity = dataset.entities.order('RANDOM()').first
        entity.node
      end

    end
  end
end

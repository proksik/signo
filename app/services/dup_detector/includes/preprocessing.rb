module DupDetector
  module Includes
    module Preprocessing

      # Properties

      def properties_from(entities)
        attributes = nil
        entities.each do |entity|
          if attributes
            attributes |= entity.attributes
          else
            attributes = entity.attributes
          end
        end
        attributes
      end

    end
  end
end

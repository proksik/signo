require 'progress_bar'

module DupDetector
  class Extracting < LOD::Importer

    def extract(dataset_name)
      dataset = DupDetector::Dataset.find_or_create_by(name: dataset_name)

      # import to neo4j
      puts "Import to graph: #{data_path_for(dataset_name)}"
      import data_path_for(dataset_name)
      puts "Import to database: #{data_path_for(dataset_name)}"
      extract_entities dataset
    end

    def extract_entities(dataset)
      entities = []
      triplets = extract_triplets data_path_for(dataset.name)

      triplets.each do |triplet|
        subject = triplet[:s]
        object = triplet[:o]

        if entity?(subject)
          entities << subject unless entities.include? subject
        end

        if entity?(object)
          entities << object unless entities.include? object
        end
      end

      dataset.entities.delete_all

      # progress bar
      progress_bar = ProgressBar.new entities.count

      entities.each do |entity_uri|
        dataset.entities.create(entity_uri: entity_uri)
        # progressbar increment
        progress_bar.increment
      end

      puts "Imported: #{entities.count}"
    end

    private

    DATA_PATH = 'data/dup_detector/'

    def data_path_for(dataset_name)
      "#{DATA_PATH}#{dataset_name}/data.nt"
    end

    def entity?(entity_uri)
      entity_uri.start_with? "<http://"
      entity_uri.start_with?("_:") || entity_uri.start_with?("<http://")
    end

  end
end

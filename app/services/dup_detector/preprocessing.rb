require 'progress_bar'

module DupDetector
  class Preprocessing

    include LOD::Includes::Common
    include DupDetector::Includes::Preprocessing
    include DupDetector::Includes::Replication

    attr_accessor :dataset, :test_data, :test_properties, :replication_prefix

    def initialize(dataset_name, count_replicates, replication_prefix = nil)
      @dataset = DupDetector::Dataset.find_or_create_by(name: dataset_name)
      @count_replicates = count_replicates
      @replication_prefix = replication_prefix
    end

    def preprocessing
      generate_temp_entities  @dataset, @count_replicates, @replication_prefix
      @dataset.reload
      generate_pairs(@dataset)
      @train_properties = current_train_properties
      @correlation_coefficients = current_coefficients
    end

    def current_coefficients
      cc = CorrelCoeff.new correl_coeff_model_name
      sgn_annota = SGN::DupDetector.new @train_properties

      # progress bar
      progress_bar = ProgressBar.new train_data.count

      labels = []
      coefficients_data = []
      train_data.each_with_index do |entity_sim, index|
        entity_a = entity_sim[:temp_entity].node
        entity_b = entity_sim[:entity].node

        labels << (entity_sim[:similar] ? 1.0 : 0.0)
        coefficients_data << sgn_annota.snp_features(entity_a, entity_b)

        # progressbar increment
        progress_bar.increment
      end

      cc.calculate_and_save coefficients_data, labels, sgn_annota.properties
    end

    def train_sgn
      svm_service = SVM.new(svm_model_name('sgn'))

      sgn_annota = SGN::DupDetector.new @train_properties, @correlation_coefficients, true
      examples = []
      labels = []

      # progress bar
      progress_bar = ProgressBar.new train_data.count

      train_data.each_with_index do |entity_sim, index|
        entity_a = entity_sim[:temp_entity].node
        entity_b = entity_sim[:entity].node

        if entity_sim[:similar]
          labels << 1.0
        else
          labels << 0.0
        end

        examples << sgn_annota.sgn_features(entity_a, entity_b)

        # progressbar increment
        progress_bar.increment
      end

      svm_service.train_with_probability(examples, labels)
    end

    def train_data
      @dataset.temp_entities_pairs.includes(:temp_entity, :entity).all.map { |entity_sim| {temp_entity: entity_sim.temp_entity, entity: entity_sim.entity, similar: entity_sim.temp_entity.source_entity.eql?(entity_sim.entity)} }
    end


    def current_train_properties
      # properties from file
      cc = CorrelCoeff.new correl_coeff_model_name
      return cc.properties if cc.exists?
      properties_from @dataset.temp_entities.map { |entity| entity.source_entity.node }
    end

    private

    def svm_model_name(type)
      "dup_detector_#{@dataset.name}_#{type}"
    end

    def correl_coeff_model_name
      "dup_detector_#{@dataset.name}"
    end

  end
end

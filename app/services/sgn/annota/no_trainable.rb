module SGN
  module Annota
    class NoTrainable < SGN::Core::Service

      SNP_WEIGHT = 2
      DRN_WEIGHT = 1
      ND_WEIGHT = 0.5
      SRN_WEIGHT = 0.5
      MIN_DEPTH = 0
      MAX_DEPTH = 2
      MIN_DIST = 2
      MAX_DIST = 4
      IGNORE_PROPERTIES = [:uri]

      def initialize(use_cache = false)
        super(ignore_properties: IGNORE_PROPERTIES, snp_weight: SNP_WEIGHT, drn_weight: DRN_WEIGHT, nd_weight: ND_WEIGHT, srn_weight: SRN_WEIGHT, max_dist: MAX_DIST, min_dist: MIN_DIST, max_depth: MAX_DEPTH, min_depth: MIN_DEPTH, use_cache: use_cache)
      end

      def weight_of_property(property_name)
        1.0
      end

    end
  end
end

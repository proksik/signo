module SGN
  module DBLP
    class Service < SGN::Core::Service

      SNP_WEIGHT = 0
      DRN_WEIGHT = 1
      ND_WEIGHT = 0.5
      MIN_DEPTH = 2
      MAX_DEPTH = 5
      MIN_DIST = 4
      MAX_DIST = 6
      SIMILAR_RATIO = 0.5

      attr_accessor :years

      def initialize
        super(snp_weight: SNP_WEIGHT, drn_weight: DRN_WEIGHT, nd_weight: ND_WEIGHT, max_dist: MAX_DIST, min_dist: MIN_DIST, max_depth: MAX_DEPTH, min_depth: MIN_DEPTH)
        @years = Neography::Node.find('types_index', :type, :year).map { |y| y.neo_id }
      end

      def similar?(node_1, node_2)
        sgn(node_1, node_2) >= SIMILAR_RATIO
      end

      def sgn(node_1, node_2, components = [:snp, :drn, :nd])
        super node_1, node_2, components
      end

      def nodes_distance(node_1, node_2, max_depth = 5)
        neo = Neography::Rest.new
        relationships = neo.get_paths(node_1, node_2, [{type: 'articles', direction: 'all'},
                                                       {type: 'in_collections', direction: 'all'},
                                                       {type: 'in_proceedings', direction: "all"},
                                                       {type: 'authors', direction: 'all'},
                                                       {type: 'book', direction: 'all'}], max_depth, 'shortestPath')
        return max_depth.to_f if relationships.empty?
        count = relationships.first['nodes'].count
        nodes = relationships.first['nodes'].map { |n| n.split('/').last }
        count+= 1 unless (nodes & @years).empty?
        (count-1).to_f
      end
    end
  end
end

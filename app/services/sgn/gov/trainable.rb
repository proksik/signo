require 'svm'

module SGN
  module GOV
    class Trainable < SGN::Core::Service
      include SGN::GOV::Includes::Common

      PROPERTIES = [:name, :address, :district_name, :type]
      IGNORE_PROPERTIES = [:valid_from, :valid_to, :ico]
      MIN_DIST = 2
      MAX_DIST = 5
      MIN_DEPTH = 0
      MAX_DEPTH = 4
      NAIVE_PROPERTY_NAME_1 = :name
      NAIVE_PROPERTY_NAME_2 = :address
      NAIVE_WEIGHT = 0.8

      attr_accessor :properties_model, :sgn_model

      def initialize(prop_model_name = nil, sgn_model_name = nil)
        @properties_model = SVM::Service.new(prop_model_name)
        @sgn_model = SVM.new(sgn_model_name)
        super(ignore_properties: IGNORE_PROPERTIES, min_dist: MIN_DIST, max_dist: MAX_DIST, min_depth: MIN_DEPTH, max_depth: MAX_DEPTH)
      end

      def snp_features(node_1, node_2)
        features = []
        snp_similarities = snp_similarities(node_1, node_2)
        PROPERTIES.each do |property_name|
          features << snp_similarities[property_name]
        end
        features
      end

      def sgn_features(node_1, node_2)
        [snp(node_1, node_2), drn(node_1, node_2), nd(node_1, node_2)]
      end

      def sgn(node_1, node_2)
        @sgn_model.predict_probability sgn_features(node_1, node_2)
      end

      def similar?(node_1, node_2, only_snp = false)
        if only_snp
          @properties_model.predict_label(snp_features(node_1, node_2)) == 1.0
        else
          @sgn_model.predict_label(sgn_features(node_1, node_2)) == 1.0
        end
      end

      def snp(node_1, node_2)
        @properties_model.predict_probability snp_features(node_1, node_2)
      end

      def similar_naive?(node_1, node_2)
        (properties_similarity(node_1[NAIVE_PROPERTY_NAME_1], node_2[NAIVE_PROPERTY_NAME_1]) >= NAIVE_WEIGHT) \
          and
            (properties_similarity(node_1[NAIVE_PROPERTY_NAME_2], node_2[NAIVE_PROPERTY_NAME_2]) >= NAIVE_WEIGHT)
      end

    end
  end
end

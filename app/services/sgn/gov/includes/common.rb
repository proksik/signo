require 'extend_string'
require 'text_similarity'

module SGN
  module GOV
    module Includes
      module Common

        def similarity_of_organizations(organization_1_id, organization_2_id)
          node_1 = ::GOV::Organization.find(organization_1_id)
          node_2 = ::GOV::Organization.find(organization_2_id)
          sgn(node_1, node_2)
        end

      end
    end
  end
end

module SGN
  module GOV
    class NoTrainable < SGN::Core::Service
      include SGN::GOV::Includes::Common

      SNP_WEIGHT = 1.0
      DRN_WEIGHT = 0.5
      ND_WEIGHT = 0.5
      PROPERTIES_WEIGHT = {name: 1.0, type: 1.0, address: 1.0, district_name: 0.5}
      IGNORE_PROPERTIES = [:valid_from, :valid_to, :ico]
      MAX_DIST = 5

      def initialize
        super(snp_weight: SNP_WEIGHT, drn_weight: DRN_WEIGHT, nd_weight: ND_WEIGHT, ignore_properties: IGNORE_PROPERTIES, properties_weight: PROPERTIES_WEIGHT, max_dist: MAX_DIST)
      end

      def sgn(node_1, node_2, components = [:snp, :drn, :nd])
        super node_1, node_2, components
      end

    end
  end
end

require 'arrays_cache'

module SGN
  module Core
    class Service
      include Includes::PropertiesSimilarity
      include Includes::RelationshipsSimilarity
      include Shared::Neo4jHelper

      # weights
      attr_accessor :snp_weight, :drn_weight, :nd_weight, :srn_weight
      # properties constants
      attr_accessor :ignore_properties, :properties_weight
      # relationships constants
      attr_accessor :max_dist, :min_dist, :max_depth

      # use caching
      attr_accessor :cache_distances

      def initialize(options = {})
        options = {min_dist: 0, max_dist: 5, ignore_properties: [], snp_weight: 0.0, drn_weight: 0.0, nd_weight: 0.0, srn_weight: 0.0, properties_weight: {}}.merge options
        @snp_weight = options[:snp_weight]
        @drn_weight = options[:drn_weight]
        @nd_weight = options[:nd_weight]
        @srn_weight = options[:srn_weight]
        @ignore_properties = options[:ignore_properties]
        @properties_weight = options[:properties_weight]
        @max_dist = options[:max_dist]
        @max_depth = options[:max_depth] || options[:max_dist]
        @min_dist = options[:min_dist]
        @min_depth = options[:min_depth] || 0
        @cache_distances = if options[:use_cache]
                             ArraysCache.new
                           else
                             nil
                           end
      end

      def sgn(node_1, node_2, components = [:snp, :drn, :nd, :srn])
        return 0.0 if [@snp_weight, @drn_weight, @nd_weight, @srn_weight].sum == 0
        v_snp = components.include?(:snp) ? snp(node_1, node_2) : 0.0
        v_drn = components.include?(:drn) ? drn(node_1, node_2) : 0.0
        v_nd = components.include?(:nd) ? nd(node_1, node_2) : 0.0
        v_srn = components.include?(:srn) ? srn(node_1, node_2) : 0.0
        sgn_by_components(v_snp, v_drn, v_nd, v_srn, components)
      end

      def sgn_by_components(v_snp, v_drn, v_nd, v_srn, components)
        top = 0.0
        top+= @snp_weight * v_snp if components.include? :snp
        top+= @drn_weight * v_drn if components.include? :drn
        top+= @nd_weight * v_nd if components.include? :nd
        top+= @srn_weight * v_srn if components.include? :srn
        bottom = 0.0
        bottom+= @snp_weight if components.include? :snp
        bottom+= @drn_weight if components.include? :drn
        bottom+= @nd_weight if components.include? :nd
        bottom+= @srn_weight if components.include? :srn
        top / bottom
      end

      # similarity of nodes properties
      def snp(node_1, node_2)
        total_similarity = 0.0
        weights = 0.0
        similarities = snp_similarities(node_1, node_2)
        similarities.each_pair do |property_name, similarity|
          weight = weight_of_property(property_name)
          total_similarity+= weight * similarity
          weights+= weight
        end
        return 0 if weights == 0.0
        total_similarity / weights.to_f
      end

      # get similarities of attributes
      def snp_similarities(node_1, node_2)
        similarities = {}
        node_1.attributes.each do |property_name|
          if !@ignore_properties.include?(property_name) && node_2.attributes.include?(property_name)
            similarities[property_name] = properties_similarity(node_1[property_name], node_2[property_name])
          end
        end
        similarities
      end

      # weight of property
      def weight_of_property(property_name)
        return 0 unless @properties_weight.include? property_name.to_sym
        @properties_weight[property_name.to_sym]
      end

      # distance between relationship-nodes

      def drn(node_1, node_2)
        nodes_from_1 = node_1.both.map { |node| node }
        nodes_from_2 = node_2.both.map { |node| node }
        dist_total = 0
        nodes_from_1.each do |node_from_1|
          min_dist = @max_depth
          nodes_from_2.each do |node_from_2|
            dist = nodes_distance(node_from_1, node_from_2, @max_depth)
            min_dist = dist if dist < min_dist
          end
          dist_total+= min_dist
        end
        normalize_distance(dist_total / nodes_from_1.count.to_f, @min_depth, @max_depth)
      end

      # distance of nodes

      def nd(node_1, node_2)
        normalize_distance(nodes_distance(node_1, node_2, @max_dist), @min_dist, @max_dist)
      end

      # relationships nodes

      def srn(node_1, node_2)
        rels_1 = grouped_outgoing_rels(node_1) #name1: [], name2: [], ...
        rels_2 = grouped_outgoing_rels(node_2) #name1: [], name2: [], ...
        both_rel_names = rels_1.keys & rels_2.keys
        ratios = 0.0
        both_rel_names.each do |rel_type|
          ratios += jaccard_ratio_for rels_1[rel_type].map(&:neo_id), rels_2[rel_type].map(&:neo_id)
        end
        ratio_for_keys = jaccard_ratio_for(rels_1.keys, rels_2.keys)
        return ratio_for_keys if both_rel_names.count == 0
        (ratios / both_rel_names.count.to_f) * ratio_for_keys
      end

    end
  end
end

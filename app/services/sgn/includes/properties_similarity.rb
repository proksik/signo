require 'extend_string'
require 'text_similarity'

module SGN
  module Includes
    module PropertiesSimilarity
      include TextSimilarity

      NUMBER_RANGE = 100

      def properties_similarity(property_value_1, property_value_2)
        if property_value_1.is_a?(Integer) && property_value_2.is_a?(Integer)
          number_similarity(property_value_1, property_value_2, NUMBER_RANGE)
        else
          text_similarity(property_value_1.to_s, property_value_2.to_s)
        end
      end

      def text_similarity(str_1, str_2)
        ( n_gram_ratio(str_1, str_2) + levenshtein_ratio(str_1, str_2) ) / 2.0
      end

      def number_similarity(num_1, num_2, range)
        diff = (num_1 - num_2).abs.to_f
        (range-diff)/range.to_f
      end

    end
  end
end

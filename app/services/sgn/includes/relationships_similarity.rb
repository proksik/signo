module SGN
  module Includes
    module RelationshipsSimilarity

      def nodes_distance(node_1, node_2, max_depth = 5)
        # if not set caching
        return nodes_distance_base(node_1, node_2, max_depth) unless @cache_distances
        key = cache_key_for(node_1, node_2, max_depth)
        distance = @cache_distances.get(key)
        return distance if distance
        distance = nodes_distance_base(node_1, node_2, max_depth)
        @cache_distances.set(key, distance)
        distance
      end

      def nodes_distance_base(node_1, node_2, max_depth = 5)
        p = node_1.shortest_path_to(node_2).depth(max_depth).nodes.first
        return max_depth.to_f unless p
        (p.count-1).to_f
      end

      def normalize_distance(dist, min_dist = 0.0, max_dist = 5.0)
        return 1.0 if dist <= min_dist
        return 0.0 if dist >= max_dist
        (max_dist.to_f - dist.to_f) / (max_dist.to_f - min_dist.to_f)
      end

      private

      def cache_key_for(node_1, node_2, max_depth)
        ids = [node_1.neo_id.to_i, node_2.neo_id.to_i]
        [ids.min, ids.max, max_depth]
      end

    end
  end
end

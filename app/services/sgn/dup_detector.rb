require 'svm'

module SGN
  class DupDetector < SGN::Core::Service

    MIN_DEPTH = 0
    MAX_DEPTH = 2
    MIN_DIST = 2
    MAX_DIST = 4
    IGNORE_PROPERTIES = [:uri]

    attr_accessor :sgn_model, :properties

    def initialize(properties = [], properties_weight = {}, use_cache = false, sgn_model_name = nil)
      @sgn_model = SVM.new(sgn_model_name)
      @properties = properties - IGNORE_PROPERTIES
      super(ignore_properties: IGNORE_PROPERTIES, min_dist: MIN_DIST, max_dist: MAX_DIST, min_depth: MIN_DEPTH, max_depth: MAX_DEPTH, use_cache: use_cache, properties_weight: properties_weight)
    end

    def snp_features(node_1, node_2)
      features = []
      snp_similarities = snp_similarities(node_1, node_2)
      @properties.each do |property_name|
        features << snp_similarities[property_name] || 0.0
      end
      features
    end

    def sgn_features(node_1, node_2)
      [snp(node_1, node_2), drn(node_1, node_2), nd(node_1, node_2), srn(node_1, node_2)]
    end

    def sgn(node_1, node_2)
      @sgn_model.predict_probability sgn_features(node_1, node_2)
    end

    def similar?(node_1, node_2)
      @sgn_model.predict_label(sgn_features(node_1, node_2)) == 1.0
    end

  end
end

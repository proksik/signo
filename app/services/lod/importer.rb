require 'xmlsimple'

# extractor to model (Annota::Entity - uri, type), uri is unuiq
# model . EntitiesDistance .. entity_a, entity_b (to Entity model)
module LOD
  class Importer
    include LOD::Includes::Common

    def import(file_path)
      neo = Neography::Rest.new
      neo.create_node_index(ENTITY_INDEX)
      triplets = extract_triplets(file_path)
      triplets.each_with_index do |triplet, index|
        subject = find_or_create_in_neo4j(triplet[:s])
        predicate = triplet[:p]

        if entity? triplet[:o]
          object = find_or_create_in_neo4j(triplet[:o])
          subject.outgoing(triplet[:p]) << object
        else
          property = clean_property triplet[:o]
          subject[predicate] = property
          subject.save
        end

        if index % 1000 == 0
          puts index
          sleep 2
        end
      end
    end

    protected

    def clean_property(property)
      property = property.gsub("\"", "").gsub("@en", "").gsub("@sk", "")
      property = property[0..-45].gsub("\"","").to_i if property.end_with? "^^<http://www.w3.org/2001/XMLSchema#integer>"
      property
    end

    def entity?(entity_uri)
      entity_uri.start_with? "<http://"
    end

    private

    def extract_triplets(file_path)
      f = File.open file_path
      triplets = []
      f.each_line do |line|
        triplet = line[0..-3].split(' ',3)
        triplets << {s: triplet[0], p: triplet[1], o: triplet[2]}
      end
      f.close
      triplets
    end

  end
end

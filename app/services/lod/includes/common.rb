module LOD
  module Includes
    module Common

      ENTITY_INDEX = 'entity_index'

      def find_or_create_in_neo4j(entity_uri)
        entity = Neography::Node.find(ENTITY_INDEX, :uri, entity_uri)
        return entity if entity
        entity = Neography::Node.create({uri: entity_uri})
        entity.add_to_index(ENTITY_INDEX, :uri, entity_uri)
        entity
      end

      def find_in_neo4j(entity_uri)
        Neography::Node.find(ENTITY_INDEX, :uri, entity_uri)
      end

    end
  end
end

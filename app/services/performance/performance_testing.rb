require 'arrays_cache'

module Performance
  class PerformanceTesting

    #100 - :default - 0.000472909
    #100 - :string - 0.001223908
    #1000 - :default - 0.004902027
    #1000 - :string - 0.004382957
    #10000 - :default - 0.049322634
    #10000 - :string - 0.067432028
    #100000 - :default - 0.288588608
    #100000 - :string - 0.488994958
    #1000000 - :default - 2.649829684
    #1000000 - :string - 5.147207742
    #10000000 - :default - 26.583153323
    #10000000 - :string - 53.785976876

    def self.process_arrays_cache
      counts = [100, 1000, 10000, 100000, 1000000,10000000]
      counts.each do |count|
        time = Benchmark.realtime {
          process_arrays_cache_for count, :default
        }
        puts "#{count} - :default - #{time}"
        time = Benchmark.realtime {
          process_arrays_cache_for count, :string
        }
        puts "#{count} - :string - #{time}"
      end
    end

    def self.process_arrays_cache_for(count, key_type)
      cache = ArraysCache.new key_type
      count.times do
        cache.set [rand(100), rand(100)], rand(100)
      end
      count.times do
        cache.get [rand(100), rand(100)]
      end
    end

  end
end
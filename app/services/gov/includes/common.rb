require 'csv'
require 'extend_string'

module GOV
  module Includes
    module Common

      def parse_csv(file_path)
        items = []
        CSV.foreach(file_path, headers: true, col_sep: ',') do |row|
          items << row
        end
        items
      end

      def data_keys_to_sym_and_clean(data)
        data.map { |line|
          line.inject({}) { |memo, (k, v)| memo[k.to_sym] = fix_value(v, k.to_sym); memo }
        }
      end

      def fix_value(value, key)
        return nil if value.eql? "\\N"
        return (value == 't') if ['t', 'f'].include?(value) && [:is_city].include?(key)
        return value.to_i if value.only_numbers? && ![:ico].include?(key)
        value
      end

    end
  end
end

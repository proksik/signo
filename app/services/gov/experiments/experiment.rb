module GOV
  module Experiments
    class Experiment
      include GOV::Includes::Common

      attr_accessor :data_size

      def initialize(data_size)
        @data_size = data_size
      end

      def train_properties
        sgn_gov = SGN::GOV::Trainable.new

        examples = []
        labels = []

        train_data.each do |organization_data|
          organization_id = organization_data[:id]
          depend_organization = ::GOV::DependOrganization.get_active(organization_id)
          if depend_organization
            organization_depend_id = depend_organization.extinct_organization_id
            organization_random_id = depend_organization.random_not_depend_id
            node_id = ::GOV::Organization.find(organization_id)
            true_node_id = ::GOV::Organization.find(organization_depend_id)
            false_node_id = ::GOV::Organization.find(organization_random_id)

            true_example = sgn_gov.snp_features(node_id, true_node_id)
            false_example = sgn_gov.snp_features(node_id, false_node_id)

            examples << true_example
            labels << 1.0

            examples << false_example
            labels << 0.0
          end
        end

        svm_service = SVM.new(svm_model_name('gov_properties'))
        svm_service.train_with_probability(examples, labels)

        nil
      end

      def train_sgn
        sgn_gov = SGN::GOV::Trainable.new

        examples = []
        labels = []

        train_data.each do |organization_data|
          organization_id = organization_data[:id]
          depend_organization = ::GOV::DependOrganization.get_active(organization_id)
          if depend_organization
            organization_depend_id = depend_organization.extinct_organization_id
            organization_random_id = depend_organization.random_not_depend_id
            node_id = ::GOV::Organization.find(organization_id)
            true_node_id = ::GOV::Organization.find(organization_depend_id)
            false_node_id = ::GOV::Organization.find(organization_random_id)

            true_example = sgn_gov.sgn_features(node_id, true_node_id)
            false_example = sgn_gov.sgn_features(node_id, false_node_id)

            examples << true_example
            labels << 1.0

            examples << false_example
            labels << 0.0
          end
        end

        svm_service = SVM.new(svm_model_name('gov_sgn'))
        svm_service.train_with_probability(examples, labels)

        nil
      end


      def evaluate(only_snp = false)
        true_positives, true_negatives, false_positives, false_negatives= 0, 0, 0, 0

        sgn_gov = SGN::GOV::Trainable.new(svm_model_name('gov_properties'), svm_model_name('gov_sgn'))
        test_data.each do |organization_data|
          organization_id = organization_data[:id]
          depend_organization = ::GOV::DependOrganization.get_active(organization_id)
          if depend_organization
            organization_depend_id = depend_organization.extinct_organization_id
            organization_random_id = depend_organization.random_not_depend_id
            node_id = ::GOV::Organization.find(organization_id)
            true_node_id = ::GOV::Organization.find(organization_depend_id)
            false_node_id = ::GOV::Organization.find(organization_random_id)

            # because any of extinct organization not in dataset
            if true_node_id
              if sgn_gov.similar?(node_id, true_node_id, only_snp)
                true_positives += 1
              else
                false_negatives += 1
              end
            end

            if false_node_id
              if sgn_gov.similar?(node_id, false_node_id, only_snp)
                false_positives += 1
              else
                true_negatives += 1
              end
            end
          end
        end

        # export results
        evaluation_service = Evaluation::FMeasures.new(true_positives, true_negatives, false_positives, false_negatives)
        evaluation_service.export_results("data/experiments/gov_experiment/#{@data_size}_#{(only_snp) ? 'snp' : 'snp_drn_nd'}.out")
        nil
      end

      def evaluate_naive
        true_positives, true_negatives, false_positives, false_negatives= 0, 0, 0, 0

        sgn_gov = SGN::GOV::Trainable.new(svm_model_name('gov_properties'), svm_model_name('gov_sgn'))
        test_data.each do |organization_data|
          organization_id = organization_data[:id]
          depend_organization = ::GOV::DependOrganization.get_active(organization_id)
          if depend_organization
            organization_depend_id = depend_organization.extinct_organization_id
            organization_random_id = depend_organization.random_not_depend_id
            node_id = ::GOV::Organization.find(organization_id)
            true_node_id = ::GOV::Organization.find(organization_depend_id)
            false_node_id = ::GOV::Organization.find(organization_random_id)

            # because any of extinct organization not in dataset
            if true_node_id
              if sgn_gov.similar_naive?(node_id, true_node_id)
                true_positives += 1
              else
                false_negatives += 1
              end
            end

            if false_node_id
              if sgn_gov.similar_naive?(node_id, false_node_id)
                false_positives += 1
              else
                true_negatives += 1
              end
            end
          end
        end

        # export results
        #puts "#{true_positives} #{true_negatives} #{false_positives} #{false_negatives}"
        evaluation_service = Evaluation::FMeasures.new(true_positives, true_negatives, false_positives, false_negatives)
        evaluation_service.export_results("data/experiments/gov_experiment/#{@data_size}_naive.out")
        nil
      end

      private

      def train_data
        organizations_data = data_keys_to_sym_and_clean parse_csv("data/gov/organizations#{@data_size}.csv")
        count = 0.8 * organizations_data.count
        organizations_data[0..count]
      end

      def test_data
        organizations_data = data_keys_to_sym_and_clean parse_csv("data/gov/organizations#{@data_size}.csv")
        count = 0.2 * organizations_data.count
        organizations_data[-count..-1]
      end

      def svm_model_name(name)
        "#{name}_#{@data_size}"
      end

    end
  end
end

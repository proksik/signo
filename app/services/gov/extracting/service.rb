module GOV
  module Extracting
    class Service
      include GOV::Includes::Common

      def initialize(create_indexes = true)
        if create_indexes
          neo = Neography::Rest.new
          neo.create_node_index('types_index')
          neo.create_node_index('regions_index')
          neo.create_node_index('districts_index')
          neo.create_node_index('places_index')
          neo.create_node_index('organizations_index')
          neo.create_node_index('sk_nace_codes_index')
        end
      end

      def extract_all
        extract_regions
        extract_regions_relations
        # districts
        extract_districts
        extract_districts_relations
        # places
        extract_places
        extract_places_relations
        # organizations and sk nace
        extract_sk_nace_codes
        extract_organizations('organizations1')
      end

      # SkNaceCode

      def extract_sk_nace_codes
        sk_nace_codes_data = data_keys_to_sym_and_clean parse_csv('data/gov/sk_nace_codes.csv')
        sk_nace_codes_data.each do |sk_nace_code_data|
          GOV::SkNaceCode.create(sk_nace_code_data)
        end
      end

      # Organization

      def extract_organizations(filename, limit = nil)
        limit = (limit.nil?) ? -1 : (limit-1)
        organizations_data = data_keys_to_sym_and_clean parse_csv("data/gov/#{filename}.csv")
        organizations_data[0..limit].each_with_index do |organization_data, index|
          unless ::GOV::ExtractedOrganization.find_by_organization_id(organization_data[:id])
            GOV::ExtractedOrganization.create(organization_id: organization_data[:id])
            GOV::Organization.create(organization_data)
          end
        end
      end

      # Places

      def extract_places(limit = nil)
        limit = (limit.nil?) ? -1 : (limit-1)
        places_data = data_keys_to_sym_and_clean parse_csv('data/gov/places.csv')
        places_data[0..limit].each do |place_data|
          GOV::Place.create(place_data)
        end
      end

      def extract_places_relations(limit = nil)
        limit = (limit.nil?) ? -1 : (limit-1)
        places_relations_data = data_keys_to_sym_and_clean parse_csv('data/gov/places_relations.csv')
        places_relations_data[0..limit].each do |place_relations_data|
          GOV::Place.create_relation(place_relations_data[:place_a_id], place_relations_data[:place_b_id])
        end
      end


      # Regions

      def extract_regions
        regions_data = data_keys_to_sym_and_clean parse_csv('data/gov/regions.csv')
        regions_data.each do |region_data|
          GOV::Region.create(region_data)
        end
      end

      def extract_regions_relations
        regions_relations_data = data_keys_to_sym_and_clean parse_csv('data/gov/regions_relations.csv')
        regions_relations_data.each do |region_relations_data|
          GOV::Region.create_relation(region_relations_data[:region_a_id], region_relations_data[:region_b_id])
        end
      end


      # Districts

      def extract_districts
        districts_data = data_keys_to_sym_and_clean parse_csv('data/gov/districts.csv')
        districts_data.each do |district_data|
          GOV::District.create(district_data)
        end
      end

      def extract_districts_relations
        districts_relations_data = data_keys_to_sym_and_clean parse_csv('data/gov/districts_relations.csv')
        districts_relations_data.each do |district_relations_data|
          GOV::District.create_relation(district_relations_data[:district_a_id], district_relations_data[:district_b_id])
        end
      end

      # depend organizations

      def extract_depend_organizations(limit = nil)
        limit = (limit.nil?) ? -1 : (limit-1)
        GOV::DependOrganization.delete_all
        depend_organizations_data = data_keys_to_sym_and_clean parse_csv('data/gov/dependence_organizations.csv')
        depend_organizations_data[0..limit].each do |depend_organization_data|
          GOV::DependOrganization.create(active_organization_id: depend_organization_data[:active_organization_id], extinct_organization_id: depend_organization_data[:extinct_organization_id])
        end
      end

    end
  end
end

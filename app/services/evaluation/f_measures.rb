module Evaluation
  class FMeasures

    attr_accessor :true_positives, :true_negatives, :false_positives, :false_negatives

    def initialize(true_positives, true_negatives, false_positives, false_negatives)
      @true_positives = true_positives
      @true_negatives = true_negatives
      @false_positives = false_positives
      @false_negatives = false_negatives
    end

    def precision
      @true_positives.to_f / (@true_positives.to_f + @false_positives.to_f)
    end

    def recall
      @true_positives.to_f / (@true_positives.to_f + @false_negatives.to_f)
    end

    def accuracy
      (@true_positives.to_f + @true_negatives.to_f) / (@true_positives.to_f + @true_negatives.to_f + @false_positives.to_f + @false_negatives.to_f)
    end

    def f1
      r = recall
      p = precision
      (2.0*p*r) / (p+r)
    end

    def export_results(filename)
      f = File.open("data/experiments/#{filename}", 'w')
      f.write "TP: #{@true_positives}\n"
      f.write "FP: #{@false_positives}\n"
      f.write "FN: #{@false_negatives}\n"
      f.write "TN: #{@true_negatives}\n"
      f.write "Precision: #{precision}\n"
      f.write "Recall: #{recall}\n"
      f.write "Accuracy: #{accuracy}\n"
      f.write "F1: #{f1}\n"
      f.close
    end

    def self.export_summary(filename, data)
      f = File.open("data/experiments/#{filename}", 'w')
      f.write "SGN\tPrecision\tRecall\tAccuracy\tF1\n"
      data.each_pair do |sgn, values|
        f.write  "#{sgn}\t"
        f.write "#{values[:precision]}\t#{values[:recall]}\t#{values[:accuracy]}\t#{values[:f1]}"
        f.write "\n"
      end
      f.close
    end

  end
end

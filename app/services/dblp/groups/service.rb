require 'xmlsimple'

module DBLP
  module Groups
    class Service
      include Shared::DBLPHelper

      def process_members(group_name)
        f = File.open(group_filename(group_name))
        content = f.read
        f.close

        authors = {}
        content.split("\n").each do |author_name|
          authors[author_name] = {from: nil, to: nil, active: true}
        end

        DBLP::AuthorsGroup.update_group(group_name, authors)
      end

      def process_pewe_members
        people_by_year, last_year = parse_pewe_people
        authors = {}
        people_by_year.each_pair do |year, people|
          people.each do |person|
            unless authors.include?(person)
              authors[person] = {from: nil, to: nil, active: false}
            end
            authors[person][:from] = (authors[person][:from].nil? || authors[person][:from] > year) ? year : authors[person][:from]
            authors[person][:to] = (authors[person][:to].nil? || authors[person][:to] < year) ? year : authors[person][:to]
            authors[person][:active] = (year == last_year)
          end
        end
        authors

        DBLP::AuthorsGroup.update_group('PeWe', authors)
      end

      def parse_pewe_people
        f = File.open(pewe_filename)
        content = f.read
        f.close

        people_by_year = {}
        last_year = nil

        content.split("\n").each do |line|
          items = line.split(' ')
          if items.size == 1
            last_year = items.first
            people_by_year[last_year] = []
          else
            people_by_year[last_year] << transform_to_index(items[0..1].join(' '))
          end
        end

        return people_by_year, last_year
      end

      def authors_from_groups
        authors = []
        DBLP::AuthorsGroup.all.each do |group|
          group.authors.keys.each do |author_name|
            if ::DBLP::Author.find(author_name)
              authors << author_name
            end
          end
        end
        authors.uniq
      end

      private

      DATA_PATH = 'data/dblp/group_members/'

      def pewe_filename
        "#{DATA_PATH}pewe.txt"
      end

      def group_filename(group_name)
        "#{DATA_PATH}#{group_name.downcase}.txt"
      end

    end
  end
end

require 'xmlsimple'

module DBLP
  module ParsingAndExtracting
    class Service
      include Shared::DBLPHelper

      IGNORE_FILES = %w(data/dblp_processed.xml)

      def initialize(options = {})
      end

      def find_nth_file(nth)
        files = Dir.glob(data_path('*')).select { |f| !IGNORE_FILES.include? f }
        files.sort.each_with_index do |filename, index|
          return filename if index == nth
        end
        nil
      end

      def parse_and_extract(start_with = nil, end_with=nil)
        create_neo4j_indexes

        freeze = true
        files = Dir.glob(data_path('*')).select { |f| !IGNORE_FILES.include? f }
        files.sort.each_with_index do |filename, index|
          if freeze
            freeze = false if start_with.nil? || (start_with == filename)
          end
          puts "Parse #{filename} - #{index+1}/#{files.count} - #{DateTime.now}"
          parse_file filename unless freeze
          # end with ending
          return if end_with == filename
        end
      end

      # debug

      def view_file(filename, entity=nil)
        xml_data = File.open(filename)
        data = XmlSimple.xml_in(xml_data)
        if entity
          puts data[entity].inspect
        else
          data.each_pair do |key, values|
            puts key
            puts values.count
          end
        end
      end

      def parse_file(filename)
        xml_data = File.open(filename)
        data = XmlSimple.xml_in(xml_data)
        data.each_pair do |key, values|
          extract_values(key, values)
        end
      end

      def extract_values(entity_name, values)
        model = case entity_name
                  when 'article'
                    Article
                  when 'inproceedings'
                    InProceeding
                  when 'incollection'
                    InCollection
                  when 'book'
                    Book
                  else
                    nil
                end
        if model
          puts "Total: #{entity_name} #{values.count}"
          values.each_with_index do |item, index|
            model.extract(item)
            puts index if index%1000 == 0
          end
        end
      end

      def create_neo4j_indexes
        @neo = Neography::Rest.new
        @neo.create_node_index('types_index')
        @neo.create_node_index('articles_index')
        @neo.create_node_index('authors_index')
        @neo.create_node_index('books_index')
        @neo.create_node_index('in_collections_index')
        @neo.create_node_index('in_proceedings_index')
        @neo.create_node_index('journals_index')
        @neo.create_node_index('years_index')
      end

      private

      DATA_PATH = 'data/dblp/'

      def data_path(file_number = nil)
        if file_number
          filename = '_' + file_number.to_s
        else
          filename = ''
        end
        "#{DATA_PATH}dblp#{filename}.xml"
      end

      def pewe_filename
        "#{DATA_PATH}pewe_members.txt"
      end

    end
  end
end

module DBLP
  module Includes
    module Common
      include Shared::Neo4jHelper
      include Shared::DBLPHelper

      def author_publications(author_node)
        publications = []
        publications += author_node.outgoing(:articles).map { |article| {publication: article, type: :article, rel: :articles} }
        publications += author_node.outgoing(:in_collections).map { |in_collection| {publication: in_collection, type: :in_collection, rel: :in_collections} }
        publications + author_node.outgoing(:in_proceedings).map { |in_proceeding| {publication: in_proceeding, type: :in_proceeding, rel: :in_proceedings} }
      end

      def backup_author_node(author_node)
        backup_data = backup_node(author_node)
        author_name = transform_to_index(author_node[:name])
        author = DBLP::BackupAuthor.where(name: author_name).first_or_initialize
        author.data = backup_data
        author.save
      end

      def restore_author_node(author_name)
        author = DBLP::BackupAuthor.where(name: transform_to_index(author_name)).first
        return nil unless author
        restored_data = author.data
        author_node = restore_node(restored_data)
        author_node.add_to_index('authors_index', :name, transform_to_index(author_node[:name]))
        author_node.add_to_index('types_index', :type, :author)
        author_node
      end

      def delete_author_node(author_node)
        neo = Neography::Rest.new
        neo.delete_node!(author_node.neo_id)
      end

      def create_temp_author(author_name)
        author_node = DBLP::Author.create(name: author_name)
        author_node.add_to_index('authors_index', :name, transform_to_index(author_name))
        author_node.add_to_index('types_index', :type, :author)
        DBLP::TempAuthor.create(neo_id: author_node.neo_id)
        author_node
      end

      def delete_temp_author(author_node)
        DBLP::TempAuthor.where(neo_id: author_node.neo_id).delete_all
        delete_author_node(author_node)
        author_node
      end

      def delete_all_temp_authors
        DBLP::TempAuthor.all.each do |temp_author|
          author_node = Neography::Node.load(temp_author.neo_id)
          delete_temp_author(author_node)
        end
      end

      def merge_temp_authors(author_node_1, author_node_2)
        merge_node = merge_nodes(author_node_1, author_node_2)
        DBLP::TempAuthor.delete_all(neo_id: author_node_2.neo_id)
        merge_node
      end

      def update_processed_author_data(author_name, grouped_pub_sgn, grouped_pub_naive)
        data_sgn = []
        grouped_pub_sgn.each do |group_author|
          publications = author_publications(group_author)
          data_sgn << publications.map { |publication| {neo_id: publication[:publication].neo_id, type: publication[:type]} }
        end
        data_naive = []
        grouped_pub_naive.each do |publications|
          data_naive << publications.map { |publication| {neo_id: publication[:publication].neo_id, type: publication[:type]} }
        end
        processed_author = DBLP::ProcessedAuthor.where(name: author_name).first_or_initialize
        processed_author.data_sgn = data_sgn
        processed_author.data_naive = data_naive
        processed_author.save
      end

    end
  end
end

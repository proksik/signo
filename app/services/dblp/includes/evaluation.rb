require 'enumerable/standard_deviation'
require 'enumerable/mean'

module DBLP
  module Includes
    module Evaluation

      def interval_normalization(min, max, val)
        return 1.0 if max == 1
        max-= min
        val-= min
        (max-val).to_f / max.to_f
      end

      def export_results(results_table)
        total_weight, total_sgn, total_naive = 0, 0, 0
        total_count = 0
        same_sgn, same_naive = 0, 0

        sgn_data, naive_data = [], []
        publications = []

        results_table.each do |result|
          publications_count = result[:publications_count]
          original_count = result[:original_count]
          sgn_count = result[:sgn_count]
          naive_count = result[:naive_count]

          total_sgn+= publications_count * interval_normalization(original_count, publications_count, sgn_count)
          total_naive+= publications_count * interval_normalization(original_count, publications_count, naive_count)

          same_sgn+=1 if original_count == sgn_count
          same_naive+=1 if original_count == naive_count

          total_weight+= publications_count
          total_count+= 1

          publications << publications_count

          sgn_data << (sgn_count - original_count)
          naive_data << (naive_count - original_count)
        end

        f = File.open("data/experiments/dblp_experiment/table.out", 'w')
        f.write results_table.first.keys.join("\t")
        f.write "\n"
        results_table.each do |result|
          f.write result.values.join("\t")
          f.write "\n"
        end
        f.close

        f = File.open("data/experiments/dblp_experiment/results.out", 'w')
        f.write "Evaluation_1 - Weight\n"
        f.write "SGN: #{total_sgn / total_weight}\n"
        f.write "Naive: #{total_naive / total_weight}\n"
        f.write "\n"

        f.write "Evaluation_2 - T/F\n"
        f.write "SGN: #{same_sgn}/#{total_count} = #{same_sgn.to_f / total_count.to_f}\n"
        f.write "Naive: #{same_naive}/#{total_count} = #{same_naive.to_f / total_count.to_f}\n"
        f.write "\n"

        f.write "Evaluation_3 - StandardDeviation\n"
        f.write "SGN: #{sgn_data.standard_deviation}\n"
        f.write "Naive: #{naive_data.standard_deviation}\n"
        f.write "\n"

        f.write "Analyze - Results\n"
        f.write "SGN - min: #{sgn_data.min}\n"
        f.write "SGN - max: #{sgn_data.max}\n"
        f.write "SGN - avg: #{sgn_data.mean}\n"
        f.write "Naive - min: #{sgn_data.min}\n"
        f.write "Naive - max: #{naive_data.max}\n"
        f.write "Naive - avg: #{naive_data.mean}\n"
        f.write "\n"

        f.write "Analyze - Count publications\n"
        f.write "Total publications: #{total_weight}, Total authors: #{sgn_data.count}\n"
        f.write "\n"
        f.write "min: #{publications.min}\n"
        f.write "max: #{publications.max}\n"
        f.write "avg: #{publications.mean}\n"
        f.write "\n"

        f.close
      end

    end
  end
end

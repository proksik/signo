module DBLP
  module Includes
    module GroupPublications

      def group_publications(publications, author_node_id = nil)
        publications_index = {}
        publications_authors = {}
        publications.each do |publication_obj|
          publication = publication_obj[:publication]
          publications_index[publication.neo_id.to_i] = publication_obj
          publications_authors[publication.neo_id.to_i] = publication.outgoing(:authors).map { |a| a.neo_id.to_i }
        end
        groups = join_by_authors publications_authors, author_node_id.to_i
        grouped_publications = []
        groups.each do |publications_group|
          grouped_publications << publications_group.map { |publication_id| publications_index[publication_id] }
        end
        grouped_publications
      end

      private

      @colors = []

      def authors_intersect?(authors_a, authors_b, ignore_node_id = nil)
        authors_a -= [ignore_node_id] if ignore_node_id
        authors_b -= [ignore_node_id] if ignore_node_id
        return true if authors_a.empty? && authors_b.empty?
        !(authors_a & authors_b).empty?
      end

      def dfs_component(publications_authors, visited_key, color, ignore_node_id)
        @colors[visited_key] = color
        authors = publications_authors[visited_key]
        publications_authors.each_key do |key|
          dfs_component(publications_authors, key, color, ignore_node_id) if @colors[key] == 0 && authors_intersect?(publications_authors[key], authors, ignore_node_id)
        end
      end

      def join_by_authors(publications_authors, ignore_node_id = nil)
        all_colors = 0
        @colors = {}
        publications_authors.keys.each do |key|
          @colors[key] = all_colors
        end

        publications_authors.keys.each do |key|
          if @colors[key] == 0
            all_colors+= 1
            dfs_component(publications_authors, key, all_colors, ignore_node_id)
          end
        end

        groups = []
        (1..all_colors).each do |color|
          group = []
          publications_authors.keys.each do |key|
            group << key if @colors[key] == color
          end
          groups << group
        end
        groups
      end

    end
  end
end

module DBLP
  module Experiments
    class Experiment
      include DBLP::Includes::Common
      include DBLP::Includes::GroupPublications
      include DBLP::Includes::Evaluation

      def initialize
      end

      def process_author(author_name)
        sgn_dblp = SGN::DBLP::Service.new

        delete_all_temp_authors

        # get original node and backup
        author_original_node = DBLP::Author.find(author_name)
        original_author_name = author_original_node[:name]
        backup_author_node(author_original_node)

        # get items
        publications = author_publications(author_original_node)

        # delete author
        delete_author_node author_original_node

        # process create sub-node from author
        grouped_publications_sgn = []

        grouped_publications_naive = group_publications publications
        grouped_publications_naive.each do |publications_obj|
          author_node = create_temp_author(original_author_name)
          publications_obj.each do |publication_obj|
            author_node.outgoing(publication_obj[:rel]) << publication_obj[:publication]
          end
          grouped_publications_sgn << author_node
          last_merged = author_node
          while last_merged != nil
            merge_with_index = -1
            grouped_publications_sgn.each_with_index do |local_node, index|
              if local_node != last_merged && sgn_dblp.similar?(last_merged, local_node)
                merge_with_index = index
                break
              end
            end
            if merge_with_index != -1
              last_merge_index = grouped_publications_sgn.index(last_merged)
              grouped_publications_sgn[last_merge_index] = merge_temp_authors(last_merged, grouped_publications_sgn[merge_with_index])
              last_merged = grouped_publications_sgn[last_merge_index]
              grouped_publications_sgn.delete_at(merge_with_index)
            else
              last_merged = nil
            end
          end
        end

        # update processed author data
        update_processed_author_data author_name, grouped_publications_sgn, grouped_publications_naive

        # delete all temp authors
        delete_all_temp_authors

        # restore node
        restore_author_node(author_name)
      end

      def split_author_publications(author_name)
        author_original_node = DBLP::Author.find(author_name)
        publications = author_publications(author_original_node)
        group_publications publications, author_original_node.neo_id
      end

      def process_group_members
        serv = ::DBLP::Groups::Service.new
        authors = serv.authors_from_groups
        tes = []
        authors.each do |author_name|
          author_name = transform_to_index(author_name)
          tes << author_name
          unless DBLP::ProcessedAuthor.exists?(name: author_name)
            process_author(author_name)
          end
        end
      end

      def evaluate
        results_table = []
        DBLP::ProcessedAuthor.processed.each do |processed_author|
          author_original_node = DBLP::Author.find(processed_author.name)
          publications_count = author_publications(author_original_node).count
          sgn_count = processed_author.data_sgn.count
          naive_count = processed_author.data_naive.count
          original_count = processed_author.original_count
          results_table << {original_count: original_count, sgn_count: sgn_count, naive_count: naive_count, publications_count: publications_count}
        end
        export_results results_table
      end


      # temp method for testing

      def simulate_node_reset(author_name = 'Maria Bielikova')
        backup_author_node(DBLP::Author.find(author_name))

        node = DBLP::Author.find(author_name)
        neo = Neography::Rest.new
        neo.delete_node!(node.neo_id)

        restore_author_node(author_name)
        node = DBLP::Author.find(author_name)

        puts node.neo_id
      end

    end
  end
end

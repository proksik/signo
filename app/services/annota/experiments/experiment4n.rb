require 'progress_bar'

module Annota
  module Experiments
    class Experiment4n # NoTrainable
      include LOD::Includes::Common
      include Annota::Includes::Replication
      include Annota::Includes::Evaluation

      # using version data set - 13_11_2013

      # 0. initialize - count_replicates, preprocessing_options
      # 1. preprocessing
      # 2. calculate_sgn
      # 3. evaluate

      DEFAULT_VERSION = '13_11_2013'

      attr_accessor :count_replicates, :preprocessing_options

      # count_replicates, generate_temp_entities: true, generate_pairs_type: :nxn
      def initialize(count_replicates, preprocessing_options = {})
        @count_replicates = count_replicates
        @preprocessing_options = {generate_temp_entities: true, generate_pairs_type: :nxn}.merge preprocessing_options
      end

      def preprocessing
        if @preprocessing_options[:generate_temp_entities]
          generate_temp_entities DEFAULT_VERSION, @count_replicates
          generate_pairs @preprocessing_options[:generate_pairs_type]
        end
      end

      def calculate_sgn
        sgn_service = SGN::Annota::NoTrainable.new true

        # progress bar
        progress_bar = ProgressBar.new Annota::TempEntitiesSimilarity.count

        Annota::TempEntitiesSimilarity.where(sgn: nil).each do |entity_sim|
          author_1 = find_or_create_in_neo4j entity_sim.temp_entity.entity_uri
          author_2 = find_or_create_in_neo4j entity_sim.entity.entity_uri

          # count sgn
          entity_sim.sgn = sgn_service.sgn(author_1, author_2)
          entity_sim.save

          progress_bar.increment
        end
        nil
      end

      SGN_SIMILARITIES = [0.5, 0.55, 0.60, 0.65, 0.70, 0.75, 0.8, 0.85, 0.90]

      def evaluate
        sgn_data = Annota::TempEntitiesSimilarity.where('sgn IS NOT NULL').map { |entity_sim| {sgn: entity_sim.sgn, similar: entity_sim.temp_entity.source_entity.eql?(entity_sim.entity)} }
        SGN_SIMILARITIES.each do |sgn_similar|
          true_positives, true_negatives, false_positives, false_negatives= 0, 0, 0, 0
          sgn_data.each do |entity_sim|
            if entity_sim[:similar] # condition positive
              if entity_sim[:sgn] >= sgn_similar # test outcome positive
                true_positives+= 1
              else # test outcome negative
                false_negatives+= 1
              end

            else # condition negative
              if entity_sim[:sgn] >= sgn_similar # test outcome positive
                false_positives += 1
              else # test outcome negative
                true_negatives += 1
              end
            end
          end
          export_results_for_fmeasures(sgn_similar, true_positives, true_negatives, false_positives, false_negatives)
        end
        data = sgn_data.map { |entity_similarity| entity_similarity[:sgn] }
        export_results_for_values(data)
      end

      def summary_results
        data = {}
        SGN_SIMILARITIES.each do |sgn_similar|
          filename = "data/experiments/#{experiment_dirname}/#{experiment_identifier}/results_#{sgn_similar.round(2).to_s}.out"
          f = File.open(filename)
          lines = f.read.split("\n")
          f.close
          precision = lines[4].split(': ').last.to_f
          recall = lines[5].split(': ').last.to_f
          accuracy = lines[6].split(': ').last.to_f
          f1 = lines[7].split(': ').last.to_f
          data[sgn_similar] = {precision: precision, recall: recall, accuracy: accuracy, f1: f1}
        end
        ::Evaluation::FMeasures.export_summary "#{experiment_dirname}/#{experiment_identifier}/summary.out", data
      end

      private

      def export_results_for_fmeasures(sgn_similar, true_positives, true_negatives, false_positives, false_negatives)
        create_directory_unless_exists("#{experiment_dirname}/")
        create_directory_unless_exists("#{experiment_dirname}/#{experiment_identifier}/")
        evaluation_service = ::Evaluation::FMeasures.new(true_positives, true_negatives, false_positives, false_negatives)
        evaluation_service.export_results("#{experiment_dirname}/#{experiment_identifier}/results_#{sgn_similar.round(2).to_s}.out")
      end

      def export_results_for_values(data)
        create_directory_unless_exists("#{experiment_dirname}/")
        create_directory_unless_exists("#{experiment_dirname}/#{experiment_identifier}/")
        export_results_values("#{experiment_dirname}/#{experiment_identifier}/values.out", data)
      end

      def experiment_identifier
        "#{@count_replicates}_#{@preprocessing_options[:generate_pairs_type]}"
      end

      def experiment_dirname
        'annota_experiment_4n'
      end

    end
  end
end

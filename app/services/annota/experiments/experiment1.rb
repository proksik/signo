module Annota
  module Experiments
    class Experiment1
      include LOD::Includes::Common
      include Annota::Includes::Evaluation

      # Using EntitiesSimilarity
      # using version data set - 13_11_2013
      # components: [:snp, :drn, :nd]

      def initialize
      end

      COUNT_CANDIDATES = 5
      DEFAULT_VERSION = '13_11_2013'

      # for each authors find 5 candidates
      def process(generated_by)
        sgn_service = SGN::Annota::NoTrainable.new

        index = 0
        Annota::Entity.from_version(DEFAULT_VERSION).where(entity_type: :author).order(:id).each do |entity_a|
          authors = find_entities(entity_a, generated_by)
          authors.each do |entity_b|
            author_1 = find_or_create_in_neo4j entity_a.entity_uri
            author_2 = find_or_create_in_neo4j entity_b.entity_uri

            # count sgn
            sgn = sgn_service.sgn(author_1, author_2, [:snp, :drn, :nd])

            # create table
            Annota::EntitiesSimilarity.create(entity_a: entity_a, entity_b: entity_b, sgn: sgn, generated_by: generated_by)
          end

          index+= 1
          puts index if index % 10 == 0
        end

      end


      def calculate_sgn(generated_by)
        sgn_service = SGN::Annota::NoTrainable.new

        index = 0
        Annota::EntitiesSimilarity.where(generated_by: generated_by, sgn: nil).each do |entity_sim|
          puts index if index % 10 == 0

          author_1 = find_or_create_in_neo4j entity_sim.entity_a.entity_uri
          author_2 = find_or_create_in_neo4j entity_sim.entity_b.entity_uri

          # count sgn
          entity_sim.sgn = sgn_service.sgn(author_1, author_2, [:snp, :drn, :nd])
          entity_sim.save

          index+= 1
        end
      end


      def evaluate(generated_by)
        data = Annota::EntitiesSimilarity.where(generated_by: generated_by).all.map { |entity_similarity| entity_similarity.sgn }
        export_results_for_experiment1 data, generated_by
      end

      private

      def find_entities(entity_a, generated_by)
        case generated_by
          when :random
            entity_a.remaining_random_entities(COUNT_CANDIDATES, generated_by)
          when :nearest
            entity_a.nearest_entities(COUNT_CANDIDATES)
          else
            []
        end
      end

      def test_nodes(neo_id_1, neo_id_2)
        sgn_service = SGN::Annota::NoTrainable.new
        author_1 = Neography::Node.load(neo_id_1)
        author_2 = Neography::Node.load(neo_id_2)
        puts sgn_service.sgn(author_1, author_2, [:snp, :drn, :nd])
      end

      def export_results_for_experiment1(data, generated_by)
        create_directory_unless_exists('annota_experiment_1/')
        export_results_values("annota_experiment_1/results_#{generated_by}.out", data)
      end

    end
  end
end

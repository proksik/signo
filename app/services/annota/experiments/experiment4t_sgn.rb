module Annota
  module Experiments
    class Experiment4tSGN < Annota::Experiments::Experiment4Trainable

      attr_accessor :correlation_coefficients

      def initialize(count_replicates, data_ratio = 0.8, preprocessing_options={})
        super count_replicates, 't_sgn', data_ratio, preprocessing_options
      end

      # preprocessing - generate_temp_entities
      #                 generate_pairs
      #                 calculate_coefficients
      # train_sgn
      # evaluate

      def preprocessing
        super
        calculate_coefficients
      end

      def train_sgn
        super SGN::Annota::TrainableSGN.new properties_from_current_authors, @correlation_coefficients, true
      end

      def evaluate
        super SGN::Annota::TrainableSGN.new properties_from_current_authors, @correlation_coefficients, true, svm_model_name('sgn')
      end

      def calculate_coefficients
        sgn_annota = SGN::Annota::TrainableSGN.new properties_from_current_authors

        # progress bar
        progress_bar = ProgressBar.new train_data.count

        labels = []
        data = []
        train_data.each_with_index do |entity_sim, index|
          temp_entity = find_or_create_in_neo4j entity_sim[:temp_entity].entity_uri
          entity = find_or_create_in_neo4j entity_sim[:entity].entity_uri

          labels << (entity_sim[:similar] ? 1.0 : 0.0)
          data << sgn_annota.snp_features(temp_entity, entity)

          # progressbar increment
          progress_bar.increment
        end

        @correlation_coefficients = correlation_coefficients_from_properties data, labels, sgn_annota.properties
      end

    end
  end
end

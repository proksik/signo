require 'progress_bar'
require 'svm'

module Annota
  module Experiments
    class Experiment4Trainable
      include LOD::Includes::Common
      include Annota::Includes::Replication
      include Annota::Includes::Evaluation
      include Annota::Includes::Preprocessing

      DEFAULT_VERSION = '13_11_2013'

      attr_accessor :count_replicates, :experiment_name, :data_ratio, :preprocessing_options

      def initialize(count_replicates, experiment_name, data_ratio = 0.8, preprocessing_options = {}) #t, t_sgn
        @count_replicates = count_replicates
        @experiment_name = experiment_name
        @data_ratio = data_ratio
        @preprocessing_options = {generate_temp_entities: true, generate_pairs_type: :nxn}.merge preprocessing_options
      end

      def preprocessing
        if @preprocessing_options[:generate_temp_entities]
          generate_temp_entities DEFAULT_VERSION, @count_replicates
          generate_pairs @preprocessing_options[:generate_pairs_type]
        end
      end

      def train_properties(sgn_annota)
        examples = []
        labels = []

        # progress bar
        progress_bar = ProgressBar.new train_data.count

        train_data.each do |entity_sim|
          temp_entity = find_or_create_in_neo4j entity_sim[:temp_entity].entity_uri
          entity = find_or_create_in_neo4j entity_sim[:entity].entity_uri

          if entity_sim[:similar]
            labels << 1.0
          else
            labels << 0.0
          end

          examples << sgn_annota.snp_features(temp_entity, entity)

          # progressbar increment
          progress_bar.increment
        end

        svm_service = SVM.new(svm_model_name('snp'))
        svm_service.train_with_probability(examples, labels)
      end

      def train_sgn(sgn_annota)
        examples = []
        labels = []

        # progress bar
        progress_bar = ProgressBar.new train_data.count

        train_data.each do |entity_sim|
          temp_entity = find_or_create_in_neo4j entity_sim[:temp_entity].entity_uri
          entity = find_or_create_in_neo4j entity_sim[:entity].entity_uri

          if entity_sim[:similar]
            labels << 1.0
          else
            labels << 0.0
          end

          examples << sgn_annota.sgn_features(temp_entity, entity)

          # progressbar increment
          progress_bar.increment
        end

        svm_service = SVM.new(svm_model_name('sgn'))
        svm_service.train_with_probability(examples, labels)
      end

      def evaluate(sgn_annota)
        true_positives, true_negatives, false_positives, false_negatives= 0, 0, 0, 0

        # progress bar
        progress_bar = ProgressBar.new test_data.count

        test_data.each do |entity_sim|

          temp_entity = find_or_create_in_neo4j entity_sim[:temp_entity].entity_uri
          entity = find_or_create_in_neo4j entity_sim[:entity].entity_uri

          if entity_sim[:similar]
            if sgn_annota.similar?(temp_entity, entity)
              true_positives += 1
            else
              false_negatives += 1
            end
          else
            if sgn_annota.similar?(temp_entity, entity)
              false_positives += 1
            else
              true_negatives += 1
            end
          end

          # progressbar increment
          progress_bar.increment
        end

        export_results_for_fmeasures true_positives, true_negatives, false_positives, false_negatives

        nil
      end

      def properties_from_current_authors
        properties_from (Annota::TempEntity.all.map { |e| e.entity_uri } + Annota::TempEntity.includes(:source_entity).all.map { |e| e.source_entity.entity_uri }).uniq.map { |entity_uri| find_in_neo4j entity_uri }
      end

      private

      # train data from Annota::TempEntitiesSimilarity
      def train_data
        sz = Annota::TempEntitiesSimilarity.count
        limit = (sz*@data_ratio).to_i
        offset = (sz* (1.0 - @data_ratio)).to_i
        Annota::TempEntitiesSimilarity.offset(offset).limit(limit).includes(:temp_entity, :entity).all.map { |entity_sim| {temp_entity: entity_sim.temp_entity, entity: entity_sim.entity, similar: entity_sim.temp_entity.source_entity.eql?(entity_sim.entity) }}
      end

      # test data from Annota::TempEntitiesSimilarity
      def test_data
        sz = Annota::TempEntitiesSimilarity.count
        limit = (sz*(1.0-@data_ratio)).to_i
        Annota::TempEntitiesSimilarity.limit(limit).includes(:temp_entity, :entity).all.map { |entity_sim| {temp_entity: entity_sim.temp_entity, entity: entity_sim.entity, similar: entity_sim.temp_entity.source_entity.eql?(entity_sim.entity) }}
      end

      def export_results_for_fmeasures(true_positives, true_negatives, false_positives, false_negatives)
        create_directory_unless_exists("#{experiment_dirname}/")
        evaluation_service = ::Evaluation::FMeasures.new(true_positives, true_negatives, false_positives, false_negatives)
        evaluation_service.export_results("#{experiment_dirname}/#{experiment_identifier}.out")
      end


      protected

      def svm_model_name(type)
        "annota_4#{@experiment_name}_#{experiment_identifier}_#{type}"
      end

      def experiment_identifier
        "#{@count_replicates}_#{@preprocessing_options[:generate_pairs_type]}"
      end

      def experiment_dirname
        "annota_experiment_4#{@experiment_name}"
      end

    end
  end
end

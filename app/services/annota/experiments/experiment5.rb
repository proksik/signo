require 'extend_string'
require 'enumerable/mean'
require 'progress_bar'
require 'correl_coeff'
require 'svm'

# prepare for new method for find duplicates
module Annota
  module Experiments
    class Experiment5
      include LOD::Includes::Common
      include Annota::Includes::Preprocessing
      include Annota::Includes::Replication
      include Annota::Includes::Evaluation

      # using version data set - 25_03_2014

      attr_accessor :entities, :test_data, :test_properties, :train_properties, :preprocessing_options

      DEFAULT_VERSION = '25_03_2014'

      def initialize(count_replicates, preprocessing_options = {})
        @entities = current_entities
        @test_data = current_test_data
        @count_replicates = count_replicates
        @preprocessing_options = {generate_temp_entities: true, generate_pairs_type: :nxn}.merge preprocessing_options
      end

      def preprocessing
        if @preprocessing_options[:generate_temp_entities]
          generate_temp_entities DEFAULT_VERSION, @count_replicates
          generate_pairs @preprocessing_options[:generate_pairs_type]
        end
        @train_properties = current_train_properties
        @test_properties = current_test_properties
        @correlation_coefficients = current_coefficients
      end


      def train_sgn
        # end if exits model
        svm_service = SVM.new(svm_model_name('sgn'))
        return if svm_service.exists?

        sgn_annota = SGN::Annota::TrainableSGN.new @train_properties, @correlation_coefficients, true
        examples = []
        labels = []

        # progress bar
        progress_bar = ProgressBar.new train_data.count

        train_data.each_with_index do |entity_sim, index|
          entity_a = entity_sim[:temp_entity].node
          entity_b = entity_sim[:entity].node

          if entity_sim[:similar]
            labels << 1.0
          else
            labels << 0.0
          end

          examples << sgn_annota.sgn_features(entity_a, entity_b)

          # progressbar increment
          progress_bar.increment
        end

        svm_service.train_with_probability(examples, labels)
      end

      def evaluate
        sgn_annota = SGN::Annota::TrainableSGN.new @test_properties, @correlation_coefficients, true, svm_model_name('sgn')
        true_positives, true_negatives, false_positives, false_negatives= 0, 0, 0, 0

        # progress bar
        progress_bar = ProgressBar.new test_data.count

        test_data.each_with_index do |entity_sim, index|
          entity_a = entity_sim[:entity_a]
          entity_b = entity_sim[:entity_b]

          if entity_sim[:similar]
            if sgn_annota.similar?(entity_a, entity_b)
              true_positives += 1
            else
              false_negatives += 1
            end
          else
            if sgn_annota.similar?(entity_a, entity_b)
              false_positives += 1
            else
              true_negatives += 1
            end
          end

          # progressbar increment
          progress_bar.increment
        end

        export_results_for_fmeasures true_positives, true_negatives, false_positives, false_negatives

        nil
      end

      def train_data
        Annota::TempEntitiesSimilarity.all.includes(:temp_entity, :entity).all.map { |entity_sim| {temp_entity: entity_sim.temp_entity, entity: entity_sim.entity, similar: entity_sim.temp_entity.source_entity.eql?(entity_sim.entity)} }
      end

      private

      def current_train_properties
        # properties from file
        cc = CorrelCoeff.new correl_coeff_model_name
        return cc.properties if cc.exists?
        properties_from Annota::TempEntity.all.map { |entity| entity.source_entity.node }
      end

      def current_test_properties
        properties_from @entities.values # only entities
      end

      def current_entities
        entities = {}
        author_names.each do |original_author_name|
          duplicates = duplicates_for_file duplicates_file_path(author_name(original_author_name), DEFAULT_VERSION)
          duplicates.each do |entity_uri|
            entities[entity_uri] = find_in_neo4j entity_uri
          end
        end
        entities
      end

      def current_test_data
        data = []
        author_names.each do |original_author_name_1|
          duplicates_1 = duplicates_for_file duplicates_file_path(author_name(original_author_name_1), DEFAULT_VERSION)
          author_names.each do |original_author_name_2|
            duplicates_2 = duplicates_for_file duplicates_file_path(author_name(original_author_name_2), DEFAULT_VERSION)
            duplicates_1.each do |entity_uri_a|
              duplicates_2.each do |entity_uri_b|
                if entity_uri_a != entity_uri_b
                  entity_a = @entities[entity_uri_a]
                  entity_b = @entities[entity_uri_b]
                  data << {entity_a: entity_a, entity_b: entity_b, similar: original_author_name_1 == original_author_name_2}
                end
              end
            end
          end
        end
        data
      end

      def current_coefficients
        cc = CorrelCoeff.new correl_coeff_model_name
        return cc.coefficients if cc.exists?
        sgn_annota = SGN::Annota::TrainableSGN.new @train_properties

        # progress bar
        progress_bar = ProgressBar.new train_data.count

        labels = []
        coefficients_data = []
        train_data.each_with_index do |entity_sim, index|
          entity_a = entity_sim[:temp_entity].node
          entity_b = entity_sim[:entity].node

          labels << (entity_sim[:similar] ? 1.0 : 0.0)
          coefficients_data << sgn_annota.snp_features(entity_a, entity_b)

          # progressbar increment
          progress_bar.increment
        end

        cc.calculate_and_save coefficients_data, labels, sgn_annota.properties
      end

      def export_results_for_fmeasures(true_positives, true_negatives, false_positives, false_negatives)
        create_directory_unless_exists("#{experiment_dirname}/")
        evaluation_service = ::Evaluation::FMeasures.new(true_positives, true_negatives, false_positives, false_negatives)
        evaluation_service.export_results("#{experiment_dirname}/results_#{experiment_identifier}.out")
      end

      def svm_model_name(type)
        "annota_5_#{experiment_identifier}_#{type}"
      end

      def correl_coeff_model_name
        "annota_5_#{experiment_identifier}"
      end

      def experiment_identifier
        "#{@count_replicates}_#{@preprocessing_options[:generate_pairs_type]}"
      end

      def experiment_dirname
        'annota_experiment_5'
      end

    end
  end
end

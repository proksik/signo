module Annota
  module Experiments
    class Experiment3
      include LOD::Includes::Common
      include Annota::Includes::Evaluation

      # Performance
      # Calculate performance test
      # using version data set - 13_11_2013
      # all components - each combinations

      COUNT_AUTHORS = 10
      DEFAULT_VERSION = '13_11_2013'

      def components_combinations
        Annota::EntitiesComponentSimilarity.components_combinations
      end

      LIMIT_FOR_ALL_COMPONENTS = 5000
      MAX_PAIRS = 50000

      def process_performance(use_cache = false)
        # generate random pairs
        pairs = []
        Annota::Entity.from_version(DEFAULT_VERSION).where(entity_type: :author).order("RANDOM()").limit(MAX_PAIRS / COUNT_AUTHORS).each_with_index do |entity_a, index|
          authors = find_entities(entity_a)
          authors.each do |entity_b|
            author_1 = find_or_create_in_neo4j entity_a.entity_uri
            author_2 = find_or_create_in_neo4j entity_b.entity_uri
            pairs << [author_1, author_2]
          end
          sleep 10 if index % 100 == 0
        end
        # calculate sgn
        results = {}
        [10, 50, 100, 500, 1000, 5000, 10000, 50000].each do |count_pairs|
          puts count_pairs
          results[count_pairs] = []
          components_combinations.each do |components|
            if count_pairs <= LIMIT_FOR_ALL_COMPONENTS || components == components_combinations.last
              results[count_pairs] << performance_for(pairs[0..(count_pairs-1)], components, use_cache)
              #sleep 60 # only after performance testing
            else
              results[count_pairs] << 0.0
            end
            puts components.inspect
          end
        end
        evaluate results, use_cache
      end

      def performance_for(pairs, components, use_cache)
        sgn_service = SGN::Annota::NoTrainable.new use_cache
        Benchmark.realtime {
          pairs.each do |pair|
            sgn_service.sgn(pair.first, pair.last, components)
          end
        }
      end

      private

      def find_entities(entity_a)
        Annota::Entity.where(entity_type: :author).from_version(DEFAULT_VERSION).where("id != ?", entity_a.id).order("RANDOM()").limit(COUNT_AUTHORS)
      end

      def evaluate(results, use_cache)
        create_directory_unless_exists('annota_experiment_3/')
        caching_name = use_cache ? "with_caching" : 'no_caching'
        create_directory_unless_exists("annota_experiment_3/#{caching_name}/")
        data = []
        results.each_pair do |count_pairs, components_time|
          time = components_time.last.to_f
          speed = time.to_f / count_pairs.to_f
          data << {count_pairs: count_pairs, time: time, speed: speed, speed10: speed*10.0, speed100: speed*100.0, speed1000: speed*1000}
        end
        export_results_performance_total("annota_experiment_3/#{caching_name}/performance_total.out", data)
        data = {}
        results.each_pair do |count_pairs, components_time|
          data[count_pairs] = components_time.map { |time| time / count_pairs.to_f }
        end
        export_results_performance_components("annota_experiment_3/#{caching_name}/performance_components.out", data, components_combinations)
        data = {}
        results.each_pair do |count_pairs, components_time|
          data[count_pairs] = components_time.map { |time| time / count_pairs.to_f * 1000.0 }
        end
        export_results_performance_components("annota_experiment_3/#{caching_name}/performance_components_1000.out", data, components_combinations)
      end

    end
  end
end

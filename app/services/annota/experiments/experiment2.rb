module Annota
  module Experiments
    class Experiment2
      include LOD::Includes::Common
      include Annota::Includes::Evaluation

      # Calculate difference in components sgn
      # Using EntitiesComponentSimilarity
      # using version data set - 13_11_2013
      # all components

      def initialize
      end

      COUNT_CANDIDATES = 5
      COUNT_AUTHORS = 2000
      DEFAULT_VERSION = '13_11_2013'

      # for each authors find 5 candidates
      def generate_pairs(generated_by)
        Annota::Entity.from_version(DEFAULT_VERSION).where(entity_type: :author).order("RANDOM()").limit(COUNT_AUTHORS).each do |entity_a|
          authors = find_entities(entity_a, generated_by)
          authors.each do |entity_b|
            # create all combinations
            Annota::EntitiesComponentSimilarity.components_combinations.each do |components|
              Annota::EntitiesComponentSimilarity.generated_by(generated_by).create(entity_a: entity_a, entity_b: entity_b, components: components, sgn: nil)
            end
          end

        end
      end

      # generate missed srn

      def generate_missed
        entities = Annota::EntitiesComponentSimilarity.group(:entity_a_id, :entity_b_id, :generated_by).select(:entity_a_id, :entity_b_id, :generated_by).reorder(:entity_a_id, :entity_b_id).map { |entity_sim| entity_sim }
        Annota::EntitiesComponentSimilarity.all.each do |entity_sim|
          components = entity_sim.components + [:srn]
          Annota::EntitiesComponentSimilarity.generated_by(entity_sim.generated_by).create(entity_a_id: entity_sim.entity_a_id, entity_b_id: entity_sim.entity_b_id, components: components, sgn: nil)
        end
        entities.each do |entity_sim|
          components = [:srn]
          Annota::EntitiesComponentSimilarity.generated_by(entity_sim.generated_by).create(entity_a_id: entity_sim.entity_a_id, entity_b_id: entity_sim.entity_b_id, components: components, sgn: nil)
        end
      end

      def calculate_sgn
        sgn_service = SGN::Annota::NoTrainable.new

        index = 0
        Annota::EntitiesComponentSimilarity.where(sgn: nil).each do |entity_sim|
          puts index if index % 10 == 0

          author_1 = find_or_create_in_neo4j entity_sim.entity_a.entity_uri
          author_2 = find_or_create_in_neo4j entity_sim.entity_b.entity_uri

          # count sgn
          entity_sim.sgn = sgn_service.sgn(author_1, author_2, entity_sim.components)
          entity_sim.save

          index+= 1
        end

        nil
      end

      def evaluate_histogram(generated_by)
        Annota::EntitiesComponentSimilarity.components_combinations.each do |components|
          data = Annota::EntitiesComponentSimilarity.generated_by(generated_by).by_components(components).where("sgn IS NOT NULL").map { |entity_similarity| entity_similarity.sgn }
          export_results_for_experiment2_values(data, generated_by, components)
        end
      end

      def evaluate_difference(generated_by)
        reference_components = Annota::EntitiesComponentSimilarity.components_combinations.last
        reference_data = Annota::EntitiesComponentSimilarity.generated_by(generated_by).where("sgn IS NOT NULL").by_components(reference_components).order(:id).map { |entity_similarity| entity_similarity.sgn }
        Annota::EntitiesComponentSimilarity.components_combinations[0..-2].each do |components|
          data = Annota::EntitiesComponentSimilarity.generated_by(generated_by).where("sgn IS NOT NULL").by_components(components).order(:id).map { |entity_similarity| entity_similarity.sgn }
          export_results_for_experiment2_difference(data, reference_data, generated_by, components)
        end
      end

      def summary_difference(generated_by)
        data = []
        Annota::EntitiesComponentSimilarity.components_combinations[0..-2].each do |components|
          filename = "data/experiments/annota_experiment_2/#{generated_by}/difference_#{filename_for_components(components)}.out"
          f = File.open filename
          items = f.read
          f.close
          items = items.split("\n")
          mean = items[1].split(': ').last.to_f.round(5)
          deviation = items[2].split(": ").last.to_f.round(5)
          min = items[3].split(": ").last.to_f.round(5)
          max = items[4].split(": ").last.to_f.round(5)
          data << {mean: mean, min: min, max: max, deviation: deviation}
        end
        export_results_summary "annota_experiment_2/#{generated_by}/summary_difference.out", data, Annota::EntitiesComponentSimilarity.components_combinations[0..-2]
      end

      def summary_results(generated_by)
        data = []
        Annota::EntitiesComponentSimilarity.components_combinations[0..-1].each do |components|
          filename = "data/experiments/annota_experiment_2/#{generated_by}/results_#{filename_for_components(components)}.out"
          f = File.open filename
          items = f.read
          f.close
          items = items.split("\n")
          mean = items[1].split(': ').last.to_f.round(5)
          deviation = items[2].split(": ").last.to_f.round(5)
          min = items[3].split(": ").last.to_f.round(5)
          max = items[4].split(": ").last.to_f.round(5)
          data << {mean: mean, min: min, max: max, deviation: deviation}
        end
        export_results_summary "annota_experiment_2/#{generated_by}/summary_results.out", data, Annota::EntitiesComponentSimilarity.components_combinations[0..-1]
      end

      private

      def find_entities(entity_a, generated_by)
        case generated_by
          when :random
            Annota::Entity.from_version(DEFAULT_VERSION).where("id != ?", entity_a.id).order("RANDOM()").limit(COUNT_CANDIDATES)
          when :nearest
            entity_a.nearest_entities(COUNT_CANDIDATES)
          else
            []
        end
      end

      def export_results_for_experiment2_values(data, generated_by, components)
        create_directory_unless_exists('annota_experiment_2/')
        create_directory_unless_exists("/annota_experiment_2/#{generated_by}/")
        export_results_values("annota_experiment_2/#{generated_by}/results_#{filename_for_components(components)}.out", data)
      end

      def export_results_for_experiment2_difference(data, reference_data, generated_by, components)
        create_directory_unless_exists('annota_experiment_2/')
        create_directory_unless_exists("annota_experiment_2/#{generated_by}/")
        difference_data = data.map.with_index { |v, index| (reference_data[index] - v).abs }
        export_results_difference("annota_experiment_2/#{generated_by}/difference_#{filename_for_components(components)}.out", difference_data)
      end

    end
  end
end

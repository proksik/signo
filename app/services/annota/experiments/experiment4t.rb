module Annota
  module Experiments
    class Experiment4t < Annota::Experiments::Experiment4Trainable

      def initialize(count_replicates, data_ratio = 0.8, preprocessing_options = {})
        super count_replicates, 't', data_ratio, preprocessing_options
      end

      # preprocessing

      def train_properties
        super SGN::Annota::Trainable.new properties_from_current_authors, true
      end

      def train_sgn
        super SGN::Annota::Trainable.new properties_from_current_authors, true, svm_model_name('snp')
      end

      def evaluate
        super SGN::Annota::Trainable.new properties_from_current_authors, true, svm_model_name('snp'), svm_model_name('sgn')
      end

    end
  end
end

module Annota
  class Cleaner
    include Annota::Includes::Replication

    def delete_temp_nodes
      destroy_temp_nodes
      Annota::TempEntitiesSimilarity.delete_all
    end

  end
end
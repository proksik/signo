module Annota
  module Includes
    module Preprocessing

      # Properties

      def properties_from(entities)
        attributes = nil
        entities.each do |entity|
          if attributes
            attributes |= entity.attributes
          else
            attributes = entity.attributes
          end
        end
        attributes
      end


      def author_names
        f = File.open(duplicates_names_path)
        authors = f.read.split("\n")
        f.close
        authors
      end

      def author_name(original_author_name)
        original_author_name.gsub(" ", "_").remove_accents
      end

      def duplicates_for_file(file_path)
        f = File.open file_path
        duplicates = f.read.split("\n")
        f.close
        duplicates
      end

      def duplicates_file_path(author_name, default_version)
        "data/annota/duplicates/#{author_name}_#{default_version}.txt"
      end

      def duplicates_names_path
        'data/annota/duplicates/names.txt'
      end

      def duplicates_candidates_path
        'data/annota/duplicates/candidates.txt'
      end

      def annota_file(version)
        "data/annota/annota_#{version}.nt"
      end
    end
  end
end

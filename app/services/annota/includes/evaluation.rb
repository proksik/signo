require 'enumerable/standard_deviation'
require 'enumerable/mean'
require 'histogram/array'

module Annota
  module Includes
    module Evaluation

      def filename_for_components(components)
        components.map{|c| c.to_s }.join('_')
      end

      def export_results_performance_components(filename, results, components_combinations)
        f = File.open("data/experiments/#{filename}", 'w')
        f.write "Count"
        f.write "\t"
        f.write components_combinations.map { |components| filename_for_components(components) }.join("\t")
        f.write "\n"

        results.each do |count_pairs, times|
          f.write count_pairs
          f.write "\t"
          f.write times.join("\t")
          f.write "\n"
        end
        f.close
      end

      def export_results_performance_total(filename, results)
        f = File.open("data/experiments/#{filename}", 'w')
        f.write("CountPairs\tTime\tSpeed\tSpeed(10)\tSpeed(100)\tSpeed(1000)")
        f.write "\n"
        results.each do |obj|
          f.write "#{obj[:count_pairs]}\t#{obj[:time]}\t#{obj[:speed]}\t#{obj[:speed10]}\t#{obj[:speed100]}\t#{obj[:speed1000]}"
          f.write "\n"
        end
        f.close
      end

      def export_results_difference(filename, data)
        count_total = data.count
        f = File.open("data/experiments/#{filename}", 'w')
        f.write "Count: #{count_total}x2"
        f.write "\n"
        f.write "Mean: #{data.mean}"
        f.write "\n"
        f.write "StandardDeviation: #{data.standard_deviation}"
        f.write "\n"
        f.write "Min: #{data.min}"
        f.write "\n"
        f.write "Max: #{data.max}"
        f.write "\n"

        f.close
      end

      def export_results_summary(filename, data, components_combinations)
        f = File.open("data/experiments/#{filename}", 'w')
        f.write "Components\tMean\tMin\tMax\tDeviation\n"
        data.each_with_index do |line, index|
          components = components_combinations[index]
         f.write  "#{filename_for_components(components)}\t#{line[:mean]}\t#{line[:min]}\t#{line[:max]}\t#{line[:deviation]}\n"
        end
        f.close
      end

      def export_results_values(filename, data)
        count_total = data.count
        histogram = {}
        histogram[:less_05] = data.select { |v| v <= 0.5 }.count
        histogram[:more_05] = data.select { |v| v > 0.5 }.count
        histogram[:more_06] = data.select { |v| v > 0.6 }.count
        histogram[:more_07] = data.select { |v| v > 0.7 }.count
        histogram[:more_08] = data.select { |v| v > 0.8 }.count
        histogram[:more_09] = data.select { |v| v > 0.9 }.count

        f = File.open("data/experiments/#{filename}", 'w')
        f.write "Count: #{count_total}"
        f.write "\n"
        f.write "Mean: #{data.mean}"
        f.write "\n"
        f.write "StandardDeviation: #{data.standard_deviation}"
        f.write "\n"
        f.write "Min: #{data.min}"
        f.write "\n"
        f.write "Max: #{data.max}"
        f.write "\n"
        f.write "<= 0.5: #{ histogram[:less_05]} - #{histogram[:less_05].to_f / count_total.to_f}"
        f.write "\n"
        f.write ">  0.5: #{ histogram[:more_05]} - #{histogram[:more_05].to_f / count_total.to_f}"
        f.write "\n"
        f.write ">  0.6: #{ histogram[:more_06]} - #{histogram[:more_06].to_f / count_total.to_f}"
        f.write "\n"
        f.write ">  0.7: #{ histogram[:more_07]} - #{histogram[:more_07].to_f / count_total.to_f}"
        f.write "\n"
        f.write ">  0.8: #{ histogram[:more_08]} - #{histogram[:more_08].to_f / count_total.to_f}"
        f.write "\n"
        f.write ">  0.9: #{ histogram[:more_09]} - #{histogram[:more_09].to_f / count_total.to_f}"
        f.write "\n"
        begin
          histogram = data.histogram(10, bin_width: 0.1)
        rescue
          histogram = []
        end
        f.write "Histogram: #{histogram}"
        f.write "\n"
        f.close
      end

      def create_directory_unless_exists(dir_name)
        dir_name = "data/experiments/#{dir_name}"
        Dir.mkdir(dir_name) unless File.exists?(dir_name)
      end

    end
  end
end

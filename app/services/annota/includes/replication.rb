module Annota
  module Includes
    module Replication
      include Shared::Neo4jHelper

      def destroy_temp_nodes
        Annota::TempEntity.all.each do |temp_entity|
          delete_node! temp_entity.node if temp_entity.node
          temp_entity.destroy!
        end
      end

      COUNT_REPLICATES = 250

      def generate_temp_entities(version, count_replicates = COUNT_REPLICATES)
        destroy_temp_nodes
        Annota::Entity.from_version(version).where(entity_type: :author).order('RANDOM()').limit(count_replicates).each do |source_entity|
          replicate_entity(version, source_entity)
        end
        nil
      end

      def generate_pairs(type = :nxn) #types - nxn, nx10
        Annota::TempEntitiesSimilarity.delete_all
        Annota::TempEntity.all.each do |temp_entity|

          case type
            when :nx10
              source_entity = temp_entity.source_entity
              Annota::TempEntitiesSimilarity.create(temp_entity_id: temp_entity.id, entity_id: source_entity.id, sgn: nil)
              10.times do
                source_entity_2 = Annota::TempEntity.order('RANDOM()').first.source_entity
                Annota::TempEntitiesSimilarity.create(temp_entity_id: temp_entity.id, entity_id: source_entity_2.id) unless Annota::TempEntitiesSimilarity.exists?(temp_entity_id: temp_entity.id, entity_id: source_entity_2.id)
              end
            when :nxn
              Annota::TempEntity.all.each do |temp_entity_help|
                source_entity = temp_entity_help.source_entity
                Annota::TempEntitiesSimilarity.create(temp_entity_id: temp_entity.id, entity_id: source_entity.id, sgn: nil)
              end
            else
          end

        end
      end

      def replicate_entity(version, source_entity)
        entity_uri = temp_entity_uri(source_entity)
        temp_entity = source_entity.annota_temp_entities.first_or_initialize entity_uri: entity_uri
        replicate_node(version, entity_uri, source_entity.node)
        temp_entity.save
      end

      def replicate_node(version, entity_uri, source_node)
        new_node = find_or_create_in_neo4j(entity_uri)

        source_properties = node_properties(source_node)
        source_properties.each_pair do |attribute_name, value|
          if attribute_name != :uri
            if random_bool
              new_node[attribute_name] = remake_value value
            else
              new_node[attribute_name] = value
            end
          end
        end

        outgoing_rels(source_node).each do |rel|
          if random_bool
            new_node.outgoing(rel[:rel_type]) << rel[:node]
          else
            new_node.outgoing(rel[:rel_type]) << random_node(version)
          end
        end

        new_node.save
      end

      private

      def temp_entity_uri(source_entity)
        "<temp_entity_from_#{source_entity.id}>"
      end

      def random_bool
        rand(10).zero?
      end

      def remake_value(value)
        if value.is_a? Integer
          value + rand(10)
        else
          value.to_s.each_char.map { |c|
            if random_bool
              a = ((c.ord + rand(10)) % 230).chr
              a.force_encoding('UTF-8')
            else
              c
            end
          }.join
        end
      end

      def random_node(version)
        entity = Annota::Entity.from_version(version).where(entity_type: :author).order('RANDOM()').first
        entity.node
      end

    end
  end
end

require 'extend_string'

module Annota
  class Duplicates
    include Annota::Includes::Preprocessing

    DEFAULT_VERSION = '25_03_2014'

    def find_all_duplicates
      f = File.open(duplicates_candidates_path)
      names = f.read.split("\n").uniq
      f.close
      authors = []
      names.each do |name|
        authors << name if find_duplicates_for name
      end
      f = File.open(duplicates_names_path, 'w')
      f.write authors.join("\n")
      f.close
    end

    def find_duplicates_for(original_author_name)
      author_name = author_name(original_author_name)
      File.delete duplicates_file_path(author_name, DEFAULT_VERSION) if File.exists?  duplicates_file_path(author_name, DEFAULT_VERSION)
      system "{ grep '#{original_author_name}' #{annota_file DEFAULT_VERSION} | grep '#person_' | cut -d ' ' -f 1 ;
                  grep '#{original_author_name.remove_accents}' #{annota_file DEFAULT_VERSION} | grep '#person_' | cut -d ' ' -f 1 ; } | uniq > #{duplicates_file_path(author_name, DEFAULT_VERSION)}"
      if File.zero?(duplicates_file_path(author_name, DEFAULT_VERSION))
        File.delete duplicates_file_path(author_name, DEFAULT_VERSION)
        false
      else
        f = File.open duplicates_file_path(author_name, DEFAULT_VERSION)
        content = f.read
        f.close
        f = File.open duplicates_file_path(author_name, DEFAULT_VERSION), 'w'
        f.write content.split("\n").uniq.join("\n")
        f.close
        true
      end
    end

  end
end
module Annota
  module Extracting
    class Service < LOD::Importer

      def extract(version) #version date, available versions: 13_11_2013, 25_03_2014
        # import to neo4j
        import data_path_with_version(version)
        extract_entities version
      end

      def extract_entities(version)
        entities = {}
        triplets = extract_triplets data_path_with_version(version)
        triplets.each_with_index do |triplet, index|
          subject = triplet[:s]
          object = triplet[:o]
          type = :other

          if author?(subject)
            type = :author
          elsif paper?(subject)
            type = :paper
          elsif organization?(subject)
            type = :organization
          elsif institution?(subject)
            type = :institution
          end
          entities[subject] = type

          if entity?(object)
            type = nil
            if author?(object)
              type = :author
            elsif paper?(object)
              type = :paper
            elsif organization?(object)
              type = :organization
            elsif institution?(object)
              type = :institution
            end
            entities[object] = type
          end

          if index % 1000 == 0
            puts index
          end
        end

        Annota::Entity.from_version(version).delete_all

        entities.each_pair do |object, type|
          Annota::Entity.from_version(version).create(entity_uri: object, entity_type: type)
        end

        puts "Authors: #{Annota::Entity.from_version(version).where(entity_type: :author).count}"
        puts "Papers: #{Annota::Entity.from_version(version).where(entity_type: :paper).count}"
        puts "Organizations: #{Annota::Entity.from_version(version).where(entity_type: :organization).count}"
        puts "Institutions: #{Annota::Entity.from_version(version).where(entity_type: :institution).count}"
        puts "Other: #{Annota::Entity.from_version(version).where(entity_type: :other).count}"
      end

      private

      DATA_PATH = 'data/annota/'

      def data_path_with_version(version)
        "#{DATA_PATH}annota_#{version}.nt"
      end

      def entity?(entity_uri)
        entity_uri.start_with? "<http://"
        entity_uri.start_with?("_:") || entity_uri.start_with?("<http://")
      end

      def organization?(entity_uri)
        entity_uri.start_with? "_:organization_stu"
      end

      def institution?(entity_uri)
        entity_uri.start_with? "<http://annota.fiit.stuba.sk/ontology#institution"
      end

      def paper?(entity_uri)
        entity_uri.start_with? "<http://annota.fiit.stuba.sk/ontology#paper"
      end

      def author?(entity_uri)
        entity_uri.start_with? "<http://annota.fiit.stuba.sk/ontology#person_"
      end

    end
  end
end

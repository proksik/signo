module Experiments
  module DBLP
    include Shared::DBLPHelper

    def publications_by_authors(processed_author)
      processed_author.map do |publications|
        publications.map do |publication|
          publication_node = Neography::Node.load(publication[:neo_id])
          publication[:node] = publication_node
          publication[:title] = publication_node[:title]
          publication[:authors] = publication_node.outgoing(:authors).map { |n| n }
          publication
        end
      end
    end

    def authors_in_group(group_name)
      group = ::DBLP::AuthorsGroup.where(name: group_name).first
      return [] unless group
      group.authors
    end

    def group_counts
      counts = {}
      ::DBLP::AuthorsGroup.all.each do |group|
        counts[group.name] = count_exists_authors(group.authors)
      end
      counts
    end

    def count_exists_authors(authors)
      count = 0
      authors.each do |author_name, properties|
        author_name = transform_to_index(author_name)
        count +=1 if ::DBLP::Author.find(author_name)
      end
      count
    end

    def filter_exists_authors(authors)
      filtered_authors = {}
      authors.map do |author_name, properties|
        author_name = transform_to_index(author_name)
        author_node = ::DBLP::Author.find(author_name)
        if author_node
          filtered_authors[author_name] = properties.merge({name: reverse_name(author_node[:name]), neo_id: author_node.neo_id})
        end
      end
      filtered_authors
    end

    def filter_exists_authors_and_sort(authors)
      filtered_authors = filter_exists_authors(authors)
      filtered_authors.values.sort { |a, b| [a[:active] ? '0' : '1', a[:name]] <=> [b[:active] ? '0' : '1', b[:name]] }
    end

  end
end
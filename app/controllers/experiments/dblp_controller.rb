class Experiments::DblpController < ApplicationController
  expose(:author) {
    if params[:name]
      ::DBLP::Author.find_name(params[:name])
    else
      nil
    end
  }
  include Experiments::DBLP
  include Shared::DBLPHelper

  before_filter :default_group
  before_action :sidebar

  def index
    if author
      redirect_to author_experiments_dblp_index_path(params[:group], DBLP::Author.to_friendly_url(reverse_name(author[:name])))
    else
      @unprocessed_authors = DBLP::ProcessedAuthor.unprocessed.order(:name)
      @processed_authors = DBLP::ProcessedAuthor.processed.order(:name)
    end
  end

  def show
    author_node = DBLP::Author.from_friendly_url(params[:url])
    redirect_to experiments_dblp_index_path and return unless author_node
    processed_author = DBLP::ProcessedAuthor.find_or_create(author_node[:name])
    @author_name = reverse_name author_node[:name]
    @processed_author = {}
    @processed_author[:sgn] = publications_by_authors processed_author.data_sgn
    @processed_author[:naive] = publications_by_authors processed_author.data_naive
    @processed_author[:original_count] = processed_author.original_count
  end

  def update
    author_node = DBLP::Author.from_friendly_url(params[:url])
    processed_author = DBLP::ProcessedAuthor.find_or_create(author_node[:name])
    processed_author.original_count = params[:original_count] if params[:original_count]
    processed_author.save
    redirect_to author_experiments_dblp_index_path(params[:group], DBLP::Author.to_friendly_url(reverse_name(author_node[:name])))
  end

  def next_author
    processed_author = DBLP::ProcessedAuthor.next_in_group
    if processed_author
      redirect_to author_experiments_dblp_index_path(params[:group], DBLP::Author.to_friendly_url(reverse_name(processed_author.name)))
    else
      redirect_to experiments_dblp_index_path
    end
  end

  private

  def default_group
    params[:group] ||= 'PeWe'
  end

  def sidebar
    @authors_counts = group_counts
    @authors = filter_exists_authors_and_sort authors_in_group(params[:group])
  end

end
